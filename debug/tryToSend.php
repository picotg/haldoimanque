<?php

namespace uga\idoine\debug;

use uga\hallib\HTMLGenerator\SelectorGenerator;
use uga\hallib\ref\instance\InstenceSelector;
use uga\hallib\sword\Sword;
use uga\hallib\sword\TEIFile;

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * 
 * page permetant d'envoyer des fichier TEI au serveur hal en utilisant SWORD même code que pour
 * l'ajout de DOI.
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

if(isset($_POST['halid'])) {
    header('Content-Type: text/xml');
    header('Content-Encoding: UTF-8');
    header('Content-type: text/xml; charset=UTF-8');
    header('Content-disposition: attachment;filename=response.xml');
    $api = new Sword();
    $api->portail = $_POST['portail'].'/' ?? '';
    $username = $_POST['username'];
    $password = $_POST['password'];
    $halId_s = $_POST['halid'];
    $fileData = file_get_contents($_FILES['teiFile']['tmp_name']);
    $originalFile = new TEIFile($fileData);
    $resp = $api->sendTEI($originalFile, $username, $password, $halId_s);
    echo $resp;
    exit(0);
}

$instanceSelector = new InstenceSelector();
$instanceSelector->extractData('../data/instance.json');
$instanceGenerator = new SelectorGenerator($instanceSelector);
$instanceGenerator->label = 'Choisir son portail Hal';
$instanceGenerator->name = 'portail';
$instanceGenerator->addField('', 'Aucun');
$instanceGenerator->frameWorksName = 'bulma';

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Envoyer un fichier DOI</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body class="content">
<div class="container">
    <form method="post" enctype="multipart/form-data">
        <p>
            <?= $instanceGenerator->generateSelectField() ?>
        </p>
        <p>
            <label class="label" for="username">login</label>
            <input class="input" type="text" name="username" id="username">
        </p>
        <p>
            <label class="label" for="password">password</label>
            <input class="input" type="password" name="password" id="password">
        </p>
        <p>
            <label class="label" for="halid">halid</label>
            <input class="input" type="text" name="halid" id="halid">
        </p>
        <p>
            <div class="file">
            <label class="file-label">
                <input class="file-input" type="file" name="teiFile" id="teiFile">
                <span class="file-cta">
                <span class="file-label">
                    Choose a file…
                </span>
                </span>
            </label>
            </div>
        </p>
        <p>
            <input class="button is-primary" type="submit" value="Envoyer">
        </p>
    </form>
</div>
</body>
</html>