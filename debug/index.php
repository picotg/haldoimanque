<?php

namespace uga\idoine\debug;

/**
 * 
 * permet de configurer le mode débuggage
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('idoine');
session_start();

$_SESSION['debug'] ??= true;

$_SESSION['nextReturn'] ??= [];

if(isset($_POST['action'])) {
    header("Content-Type: application/json");
    $result = [
        'debug' => $_SESSION['debug'],
        'nextReturn' => $_SESSION['nextReturn'],
    ];
    switch($_POST['action']) {
        case 'setDebug':
            $_SESSION['debug'] = ($_POST['debug'] === 'true');
            $result['debug'] = $_SESSION['debug'];
            break;
        case 'addNextReturn':
            $result['nextReuturnOrder'] = array_push($_SESSION['nextReturn'], [
                'code' => intVal($_POST['code']),
                'errorMessage' => $_POST['errorMessage'],
            ]) - 1;
            $result['code'] = $_POST['code'];
            $result['errorMessage'] = $_POST['errorMessage'];
            break;
    };
    echo json_encode($result);
    exit(0);
}

$debug = $_SESSION['debug'];
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <title>Option mode debbug</title>
</head>
<body>
<div class = "container">
    <h1>Option de debuggage</h1>
    <h2>Activation</h2>
    <p>
        <label class="checkbox">
            <input type="checkbox" name="debug" id="debug"<?= $debug?' checked':'' ?>>
            activer le mode debbugage
        </label>
    </p>
    <p id="debugStatus">Le mode debbuggage est <?= $debug?'activer':'désactivé' ?></p>
    <h2>Retour en mode debug</h2>
    <h3>nouveau retoure</h3>
    <form id="addNextReturn">
        <p>
            <label class="label" for="code">code retoure</label>
            <input class="input" type="text" name="code" id="code">
        </p>
        <p>
            <label class="label" for="errorMessage">message d'erreur éventuel</label>
            <input class="input" type="text" name="errorMessage" id="errorMessage">
        </p>
        <p><input type="submit" value="ajouter"></p>
    </form>
    <h3>Tableau des prochains retoure</h3>
    <table>
        <thead>
            <tr>
                <th>numero</th>
                <th>code</th>
                <th>message d'erreur</th>
            </tr>
        </thead>
        <tbody id="listNextReturn">
            <?php foreach($_SESSION['nextReturn'] as $order => $return): ?>
                <tr>
                    <td><?= $order ?></td>
                    <td><?= $return['code'] ?></td>
                    <td><?= $return['errorMessage'] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</body>
<script>
    $('#debug').change(function() {
        $.ajax({
            type: 'post',
            data: {
                action: 'setDebug',
                debug:  this.checked
            },
            success: data => {
                status = data.debug?'activer':'désactivé';
                console.log(status)
                $('#debugStatus').html('Le mode debbuggage est ' + status);
            }
        })
    })
    $('#addNextReturn').submit(ev => {
        ev.preventDefault();
        $.ajax({
            type: 'post',
            data: {
                action: 'addNextReturn',
                code: $('#code').val(),
                errorMessage: $('#errorMessage').val()
            },
            success: data => {
                $('#listNextReturn').append('<tr><td>' + data.nextReuturnOrder + '</td><td>' + data.code + '</td><td>' + data.errorMessage + '</td></tr>')
            }
        })
    })
</script>
</html>
