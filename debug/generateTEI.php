<?php

namespace uga\idoine\debug;

use uga\hallib\sword\TEIFile;

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * 
 * page permetant de generé des fichier TEI en utilisant le même code que pour
 * l'ajout de DOI.
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

if(isset($_GET['halid'])&&isset($_GET['DOI'])) {
    $filename = $_GET['halid'];
    $filename .= (isset($_GET['new']))?'.new.tei':'.tei';
    header('Content-Type: text/xml');
    header('Content-Encoding: UTF-8');
    header('Content-type: text/xml; charset=UTF-8');
    header('Content-disposition: attachment;filename=' . $filename);
    $urlSite = "https://hal.archives-ouvertes.fr/";
    $tei = file_get_contents($urlSite.$_GET['halid']."/tei");
    if(isset($_GET['new'])) {
        $originalFile = new TEIFile($tei);
        $originalFile->delVersionNode();
        $originalFile->changeDoi($_GET['DOI']);
        $tei = $originalFile->getValue();
    }
    echo $tei;
    exit(0);
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Générateur fichier DOI</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body class="content">
<div class="container">
    <form>
        <p>
            <label class="label" for="halid">halid</label>
            <input class="input" type="text" name="halid" id="halid">
        </p>
        <p>
            <label class="label" for="DOI">DOI</label>
            <input class="input" type="text" name="DOI" id="DOI">
        </p>
        <p>
            <label class="checkbox" for="new">
                <input type="checkbox" name="new" id="new"> version avec DOI modifier
            </label>
        </p>
        <p>
            <input class="button is-primary" type="submit" value="Envoyer">
        </p>
    </form>
</div>
</body>
</html>