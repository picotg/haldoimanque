<?php

namespace uga\idoine\debug;

use uga\hallib\HTMLGenerator\SelectorGenerator;
use uga\hallib\ref\instance\InstenceSelector;

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * 
 * page permetant d'envoyer des fichier TEI au serveur hal en utilisant SWORD même code que pour
 * l'ajout de DOI.
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

$instanceSelector = new InstenceSelector();
$instanceSelector->extractData('../data/instance.json');
$instanceGenerator = new SelectorGenerator($instanceSelector);
$instanceGenerator->label = 'Choisir son portail Hal';
$instanceGenerator->name = 'portail';
$instanceGenerator->addField('', 'Aucun');
$instanceGenerator->frameWorksName = 'bulma';

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Envoyer un fichier DOI</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
</head>
<body class="content">
<div class="container">
    <form id="mainForm">
        <p>
            <?= $instanceGenerator->generateSelectField() ?>
        </p>
        <p>
            <label class="label" for="username">login</label>
            <input class="input" type="text" name="username" id="username">
        </p>
        <p>
            <label class="label" for="password">password</label>
            <input class="input" type="password" name="pass" id="pass">
        </p>
        <p>
            <label class="label" for="halid">halid</label>
            <input class="input" type="text" name="halid" id="halid">
        </p>
        <p>
            <label class="label" for="DOI">DOI</label>
            <input class="input" type="text" name="DOI" id="DOI">
        </p>
        <p>
            <input class="button is-primary" type="submit" value="Envoyer">
        </p>
    </form>
</div>
</body>
<script>
    $('#mainForm').submit(function(ev) {
        ev.preventDefault();
        $.ajax({
            type: 'post',
            url: '../api/DOIModifier.php',
            data : {
                portail: $('#portail').val(),
                password: $('#pass').val(),
                username: $("#username").val(),
                DOI: $("#DOI").val(),
                halid: $("#halid").val(),
            },
            success: async function(data) {
                saveInfo = {
                    DOI: DOI,
                    structure: data['structure'],
                    originalTEI: data['originalTEI'],
                    newTEI: data['newTEI'],
                    portail: data['portail'],
                    status: 'added',
                    date: Date.now(),
                }
                console.log(saveInfo);
            }
        })
    })
</script>
</html>