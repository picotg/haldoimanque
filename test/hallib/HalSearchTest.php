<?php

namespace uga\idoine\test\hallib;

use PHPUnit\Framework\TestCase;
use uga\idoine\search\HalSearch;

/**
 * 
 * Search Hal pour les publication sans DOI.
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */


class HallSearchTest extends TestCase {
    private static $URL_API = 'https://api.archives-ouvertes.fr/search/saga?q=structure_t:3SR&wt=json&rows=100&fl=halId_s&fq=-doiId_s:[%22%22+TO+*]&fq=docType_s:ART&sort=docid+asc';
    private static $dataDirectValue;

    public static function setUpBeforeClass(): void {
        if(!isset(static::$dataDirectValue)) {
            static::$dataDirectValue = [];
            foreach(json_decode(file_get_contents(static::$URL_API))->response->docs as $halDoc) {
                array_push(static::$dataDirectValue, $halDoc->halId_s);
            }
        }
    }
    

    public function testSimpleSearch() {
        $halSearch = new HalSearch([
            'nb_max' => 5,
            'structure' => '3SR',
            'doctype' => 'ART',
            'portail' => 'saga',
        ]);
        $halSearchData = json_decode($halSearch->data);
        foreach($halSearchData->restults as $i => $data) {
            $this->assertEquals($data->haldoc->halId_s, static::$dataDirectValue[$i]);
        }
    }

    public function testIgnoreList() {
        $halSearch = new HalSearch([
            'nb_max' => 5,
            'structure' => '3SR',
            'doctype' => 'ART',
            'portail' => 'saga',
        ]);
        $halSearch->ignoreList = [static::$dataDirectValue[0], static::$dataDirectValue[3]];
        $halSearchData = json_decode($halSearch->data);
        foreach($halSearchData->restults as $data) {
            $this->assertContains($data->haldoc->halId_s, static::$dataDirectValue);
            $this->assertNotContains($data->haldoc->halId_s, $halSearch->ignoreList);
        }
        $this->assertEquals($halSearchData->nbIgnored, 2, 'nbIgnored mal renseigner');
        $halSearch->ignoreList = [static::$dataDirectValue[0], static::$dataDirectValue[8]];
        $halSearchData = json_decode($halSearch->data);
        foreach($halSearchData->restults as $data) {
            $this->assertContains($data->haldoc->halId_s, static::$dataDirectValue);
            $this->assertNotContains($data->haldoc->halId_s, $halSearch->ignoreList);
        }
        $this->assertEquals($halSearchData->nbIgnored, 1, 'nbIgnored mal renseigner');
    }

    public function testTmpIgnore() {
        $halSearch = new HalSearch([
            'nb_max' => 5,
            'structure' => '3SR',
            'doctype' => 'ART',
            'portail' => 'saga',
        ]);
        $halSearch->tmpIgnore = [static::$dataDirectValue[0], static::$dataDirectValue[3]];
        $halSearchData = json_decode($halSearch->data);
        foreach($halSearchData->restults as $data) {
            $this->assertContains($data->haldoc->halId_s, static::$dataDirectValue);
            $this->assertNotContains($data->haldoc->halId_s, $halSearch->tmpIgnore);
        }
        $this->assertEquals($halSearchData->nbIgnored, 2, 'nbIgnored mal renseigner');
        $halSearch->tmpIgnore = [static::$dataDirectValue[0], static::$dataDirectValue[8]];
        $halSearchData = json_decode($halSearch->data);
        foreach($halSearchData->restults as $data) {
            $this->assertContains($data->haldoc->halId_s, static::$dataDirectValue);
            $this->assertNotContains($data->haldoc->halId_s, $halSearch->tmpIgnore);
        }
        $this->assertEquals($halSearchData->nbIgnored, 1, 'nbIgnored mal renseigner');
        $halSearch->tmpIgnore = [static::$dataDirectValue[0]];
        $halSearch->ignoreList = [static::$dataDirectValue[0], static::$dataDirectValue[3]];
        $halSearchData = json_decode($halSearch->data);
        foreach($halSearchData->restults as $data) {
            $this->assertContains($data->haldoc->halId_s, static::$dataDirectValue);
            $this->assertNotContains($data->haldoc->halId_s, $halSearch->ignoreList);
            $this->assertNotContains($data->haldoc->halId_s, $halSearch->tmpIgnore);
        }
        $this->assertEquals($halSearchData->nbIgnored, 2, 'nbIgnored mal renseigner');
    }

    public function testShownHalResult() {
        $halSearch = new HalSearch([
            'nb_max' => 5,
            'structure' => '3SR',
            'doctype' => 'ART',
            'portail' => 'saga',
        ]);
        $halSearch->shownHalResult = [static::$dataDirectValue[0], static::$dataDirectValue[3]];
        $halSearchData = json_decode($halSearch->data);
        foreach($halSearchData->restults as $data) {
            $this->assertContains($data->haldoc->halId_s, static::$dataDirectValue);
            $this->assertNotContains($data->haldoc->halId_s, $halSearch->shownHalResult);
        }
        $this->assertEquals($halSearchData->nbIgnored, 0, 'nbIgnored mal renseigner');
    }

    public function testDocInfo() {
        $halSearch = new HalSearch([
            'nb_max' => 5,
            'structure' => '3SR',
            'doctype' => 'ART',
            'portail' => 'saga',
        ]);
        $halSearchData = json_decode($halSearch->data);
        foreach($halSearchData->restults as $i => $data) {
            $this->assertEquals($data->haldoc->halId_s, static::$dataDirectValue[$i]);
            foreach(HalSearch::RETURNED_FIELD as $fieldName) {
                $docInfo = json_decode(HalSearch::docInfo($data->haldoc->halId_s));
                $this->assertEquals($data->haldoc->$fieldName, $docInfo->$fieldName);
            }
        }
    }
}
