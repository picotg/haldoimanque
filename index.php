<?php
namespace uga\idoine;
/**
 * 
 * index
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use uga\hallib\ref\instance\InstenceSelector;
use uga\hallib\HTMLGenerator\SelectorGenerator;

session_name('idoine');
session_start();

$_SESSION['ignorelist'] ??= [];

$instanceSelector = new InstenceSelector();
if(isset($_GET['uga'])) {
    $instanceSelector->extractData('data/instance.json');
} else {
    $instanceSelector->extractData();
}
$instanceGenerator = new SelectorGenerator($instanceSelector);
$instanceGenerator->label = 'Choisir son portail Hal';
$instanceGenerator->name = 'portail';
$instanceGenerator->addField('', 'Aucun');
$instanceGenerator->frameWorksName = 'bulma';
$index = true;
?>
<!DOCTYPE html>
<html>
<head>
    <title>idOIne</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
    <script type="text/javascript" src="js/flash_message.js"></script>
    <script src="js/docInfo.min.js"></script>
    <link rel="stylesheet" href="css/flash_message.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="type/ico" href="favicon.ico" />
</head>
<body class="content">
<?php require "navbar.php" ?>
    <div class = "container">
        <div id="mdp-dialog" title="Se connecter au portail Hal" style="display:none" class="container">
            <form id="loginForm">
                La connexion est limitée à cette requête. Se reconnecter à chaque nouvelle recherche.
                <label class="label" for="login">Login</label>
                <input class="input" id="login" name="login" type="text"><br />
                <label for="password" class="label">Mot de passe</label>
                <input class="input" id="password" name="password" type="password"><br />
                <input class="button is-primary" type="submit" value="Valider">
            </form>
        </div>
        <div id="dialog"style="display:none" class="container">
        </div>
        <h1 class="title">idOIne</h1>
        <div class="container" id="search-panel">
            <p>
                Identifie les publications sans DOI dans une structure de Hal. Propose un DOI moissonné sur Crossref,
                Istex ou DOAJ et l'intègre dans la notice Hal.
            </p>
            <p>La modification est réservée aux administrateurs de portail et aux référents structures sur leur périmètre.</p>
            <p>Les historiques ne sont conservés que dans le navigateur.</p>
            <form id="search-form">
                <div class="field">
                    <?= $instanceGenerator->generateSelectField() ?>
                </div>
                <div class="field">
                    <label class="checkbox">
                        <input id="allInstance" type="checkbox">
                        Recherche sur la totalité du protail
                    </label>
                </div>
                <div class="field">
                    <label class="label" for="structure">Choisir une structure (interroge le champs structure_t)</label>
                    <input type="text" class="input" id="structure"> <a id="searchStructure" href="#">(Trouver une structure dans AureHal structures)</a>
                </div>
                <div class="field">
                    <label class="label" for="doctype">
                        <input type="checkbox" name="addDoctypeFilter" id="addDoctypeFilter" checked>
                        Type de document
                    </label>
                    <div class="select">
                        <select name="doctype" id="doctype"></select>
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="nb_max">Nombre maximum de résultats</label>
                    <input type="number" class="input" name="nb_max" id="nb_max" value="5">
                </div>
                <div class="field">
                    <fieldset class="control box">
                        <legend class="label">Années</legend>
                        <div class="control">
                            <div class="field"><label for="producedDateIntervalAll" class="radio"><input type="radio" name="producedDateInterval" value="all" id="producedDateIntervalAll" checked> toutes</label></div>
                            <div class="field"><label for="producedDateInterval" class="radio"><input type="radio" name="producedDateInterval" value="some" id="producedDateInterval"> de <input type="number" class="input" style="width: 20%;" name="producedDateStart" id="producedDateStart" value="2019"> à <input type="number" class="input" style="width: 20%;" name="producedDateEnd" id="producedDateEnd" value="2022"></label></div>
                            <div class="field"><label for="toPub" class="radio"><input type="radio" name="producedDateInterval" value="toPub" id="toPub"> À paraitre</label></div>
                        </div>
                    </fieldset>
                </div>
                <div class="field">
                    <input class="button is-primary" type="submit" value="Chercher">
                </div>
            </form><br />
        </div>
        <a class="button is-primary" id="newSearch" href="#">Nouvelle recherche</a>
        <div id="result"></div>
    </div>
    <br><br>
    <div id="flash_messages"></div>
    <footer class="footer">
        <br><br>
        <p>Code PHP de cet outil disponible <a target="_blank" href="https://gricad-gitlab.univ-grenoble-alpes.fr/picotg/haldoimanque">ici</a> sous licence libre AGPL v3.0.</p>
    </footer>
    </body>
    <script type="text/javascript">
        var engine = new liquidjs.Liquid({
            root: ['views/'],
            extname: '.liquid'
        });
        // mémoire du mot de passe courament renseigné
        var currentPass = '';
        // list des ligne contenant un mauvais résultat
        var emptyRows = [];
        // set de document Hal
        var halDocumentSet = null;
        // DOI temporare
        var tmpDOI = [];
        // liste des données dans local storage n'étant pas des document
        const OTHER_DATA = ['portail'];

        function loadDoctypeOptions($portail='') {
            queryData = {};
            if($portail!=='') queryData['portail'] = $portail;
            $.ajax({
                url: 'api/doctypes.php',
                data: queryData,
                success: resultData => {
                    $('#doctype').empty();
                    for(doctype of resultData) {
                        $('#doctype').append($('<option/>', {
                            'value': doctype.value,
                            'text': doctype.label
                        }));
                        if('sub' in doctype) {
                            for(subdoctype of doctype.sub) {
                                $('#doctype').append($('<option/>', {
                                    'value': subdoctype.value,
                                    'text': ' -> ' + subdoctype.label
                                }));
                            }
                        }
                    }
                }
            })
        }

        if(localStorage.getItem("portail") !== null) {
            $('#portail').val(localStorage.getItem("portail"));
            loadDoctypeOptions(localStorage.getItem("portail"));
        } else {
            loadDoctypeOptions(localStorage.getItem("portail"));
        }

        $('#portail').change(function(){
            loadDoctypeOptions($('#portail').val());
        });

        init_flash_messages_observer();

        function allDone(docSet, allDone) {
            if(allDone) {
                $('.loadingEmptyRow').hide();
            }
        }

        // ajoute dans la sessions php la liste des halDocument ignorer si besoin
        function synchrinizeIgnoreListe() {
            sessionIgnoreList = JSON.parse('<?= json_encode($_SESSION['ignorelist']) ?>');
            for( let i = 0; i < localStorage.length; i++) {
                var key = localStorage.key(i);
                if(OTHER_DATA.includes(key)) continue;
                try {
                    var JSONData = JSON.parse(localStorage.getItem(key));
                    if(JSONData.status == 'ignore' || JSONData.status == 'error') {
                        $.ajax({
                            type:"post",
                            url:'api/ignorelist.php',
                            data: {
                                halid: key
                            }
                        })
                    }
                } catch(error) {
                    OTHER_DATA.push(key);
                }
            }
        }
        synchrinizeIgnoreListe();

        //
        // Gestion des dialog
        //

        // Ouverture dialog de recherche de structure
        $('#searchStructure').click(async ev => {
            ev.preventDefault();
            searchStructure = await engine.renderFile("dialog/searchStructure", {structure: $('#structure').val()});
            await $("#dialog").dialog({
                title:'Rechercher une structure',
                position: {my: 'center', at: 'center', of: window},
                width: '80%',
            });
            $("#dialog").html(searchStructure);
        })

        //
        // validation formulare
        //

        // enregistrement login
        $('#loginForm').submit(ev => {
            ev.preventDefault();
            currentPass = $('#password').val();
            $('#mdp-dialog').dialog('close');
            halDocumentSet.resendDOI($("#login").val(), currentPass);
            askAddDOIs(tmpDOI);
            tmpDOI = [];
        })

        // affiche statut de la recherche de DOI
        async function showSearchStatus(halDocument) {
            await $('#' + halDocument.halid + 'SelectedResult').empty();
            var rendedHTML = await engine.renderFile("page/result/launchedSearch", {
                halid: halDocument.halid,
                engine: halDocument.currentEngineName,
            });
            await $('#' + halDocument.halid + 'SelectedResult').append(rendedHTML);
        }

        // affiche le resultat selectionner parmis ceux trouver
        async function showFindedResult(halDocument) {
            if(halDocument.selectedResult!=null) {
                if(halDocument.isBadResult||halDocument.selectedResult === null) {
                    halDocumentSet.ignore(halDocument.halid);
                } else {
                    var halid = halDocument.halid;
                    var data = halDocument.selectedResult;
                    $('#' + halid + 'SelectedResult').empty();
                    $('#' + halid + 'ModLink').data('halid', halid);
                    data["color"] = getColor(data['scorePercent']);
                    data["halid"] = halid;
                    var resultHTML = await engine.renderFile("page/result/result", data)
                    $('#' + halid + 'SelectedResult').append(resultHTML);
                }
            }
        }

        /**
         * enléve la ligne de document qui vient d'être ignorer
         */
        async function ignore(halDocument) {
            var emptyLine = await engine.renderFile("page/result/emptyLine", {halid:halDocument.halid, emptyType: halDocument.isBadResult?'badResult':''})
            await $('#' + halDocument.halid + 'ResultRow').empty();
            await $('#' + halDocument.halid + 'ResultRow').append(emptyLine);
            emptyRows.push(halDocument.halid);
        }

        /**
         * ajout dans le tableau les document ajouter.
         */
        async function showAdded(halDocument) {
            show_flash_messages('le DOI a bien été ajouté.', 'success');
            var successAddDOI = await engine.renderFile('page/result/status/successAddDOI', {halid: halDocument.halid, DOI: halDocument.chosenDOI});
            await $("#"+halDocument.halid+"SelectedResult").html(successAddDOI);
        }

        /**
         * met à jour le nombre de document dont le DOI à était ajouter.
         */
        async function nbAddChange() {
            await $("#nbAdd").text(halDocumentSet.nbAdd);
        }

        /**
         * redemande le mot de passe et met en pose l'ajout des DOI en cas d'erreur 401.
         */
        async function error401OnAdd(halDocument) {
            $("#mdp-dialog").dialog ({
                modal: true,
                position: {my: 'center', at:"top", of: window},
            });
            currentPass = "";
            halDocumentSet.pauseAddDOI();
        }

        /**
         * affiche les erreur dans l'ajout de DOI.
         */
        async function showError(halDocument) {
            show_flash_messages('le document Hal ' + halDocument.halid + ' n\'a pas été modifier', 'danger');
            var errorAddDOI = await engine.renderFile('page/result/status/errorAddDOI', {
                halid: halDocument.halid,
                DOI: halDocument.chosenDOI,
                errorMessage: halDocument.status.errorMessage,
            });
            $("#"+halDocument.halid+"SelectedResult").html(errorAddDOI);
        }

        /**
         * observer déclancher par la fin de la recherche des document dans Hal
         * pour l'affichage des résultat et le masquage du formulaire de recherche.
         */
        async function searchFinish(halDocumentSet, data) {
            $('#search-panel').hide();
            $('#newSearch').show();
            var tableBase = await engine.renderFile("page/result/searchTable", {})
            await $('#result').append(tableBase);
            refreshCounter(halDocumentSet, data);
            $("#dialog").dialog('close');
            window.scrollTo(0, 0);
        }

        /**
         * observer pour la mise à jour de l'affichage des compteurs de documents.
         */
        async function refreshCounter(halDocumentSet, data) {
            $('#nbResult').text(data.nbResult);
            $('#nbIgnored').text(data.nbIgnored);
            $('#nbTodo').text(data.nbResult-data.nbIgnored);
        }

        /**
         * affichage des erreurs de recherche.
         */
        async function HalSearchError(halDocumentSet) {
            $("#dialog").html('La recherche d\'élément renvoie une erreur');
            $("#dialog").dialog ({
                title: 'Erreur',
                modal: true,
                position: {my:'center', at:'center', of: window},
            });
        }

        // envoie le formulaire de recherche
        $('#search-form').submit(async ev=>{
            ev.preventDefault();
            if($('#portail').val()==''&&!confirm('Êtes-vous sûr de ne pas vouloir choisir de portail?')) {
                return
            }
            if ($("#structure").val()==='' && !$('#allInstance').prop('checked')) {
                alert('Vous devez rentrer une structure.');
                return
            }
            localStorage.setItem('portail', $("#portail").val());
            halDocumentSet = new HalDocumentSet({
                addDoctypeFilter: $("#addDoctypeFilter")[0].checked,
                doctype: $("#doctype").val(),
                nbMax: $("#nb_max").val(),
                portail: $('#portail').val(),
                structure: $("#structure").val(),
                producedDateInterval: $('#producedDateInterval').prop('checked'),
                toPub: $('#toPub').prop('checked'),
                allInstance: $('#allInstance').prop('checked'),
                producedDateStart: $('#producedDateStart').val(),
                producedDateEnd: $('#producedDateEnd').val(),
            });
            halDocumentSet.documentObservers = {
                search: [showSearchStatus],
                wait: [showFindedResult],
                ignore: [ignore],
                added: [showAdded],
                error401: [error401OnAdd],
                error: [showError],
            }
            halDocumentSet.addObserver(searchFinish, 'searchFinish');
            halDocumentSet.addObserver(HalSearchError, 'HalSearchError');
            halDocumentSet.addObserver(addHalDoc, 'addHalDoc');
            halDocumentSet.addObserver(allDone, 'allDone');
            halDocumentSet.addObserver(refreshCounter, 'completeRowFinish');
            halDocumentSet.addObserver(nbAddChange, 'nbAddChange');
            halDocumentSet.searchHalDocument();
            var waitingHalResult = await engine.renderFile("dialog/waitingHalResult", {structure: $('#structure').val()});
            $("#dialog").html(waitingHalResult);
            await $("#dialog").dialog({
                title:'Rechercher en cours',
                position: {my: 'center', at: 'center', of: window},
            });
        })

        // renvoie la couleur en fonction du pourcentage de correspondance
        function getColor(score) {
            return score > 99  ? '#00ff00' :
                score > 90   ? '#55cc55' :
                score > 75  ? '#aabb55' :
                '#ff0000';
        }

        /**
         * demande validation avant modification d'un DOI
         */
        async function askAddDOIs(halidList) {
            var isList = true;
            if(!Array.isArray(halidList)) {
                halidList = [halidList];
                isList = false;
            }
            if(currentPass=='') {
                tmpDOI = halidList;
                $("#mdp-dialog").dialog ({
                    modal: true,
                    position: {my: 'center', at:"top", of: window},
                });
            } else {
                var docList = [];
                halidList.forEach( halid => {
                    if(halDocumentSet.getHalDoc(halid).chosenDOI!='') {
                        docList.push({
                            result: halDocumentSet.getHalDoc(halid).selectedResult,
                            haldoc: halDocumentSet.getHalDoc(halid).haldoc,
                            isList: isList,
                        })
                    }
                });
                if(docList.length !== 0) {
                    if(isList) {
                        var DOIValidationListDialog = await engine.renderFile("dialog/DOIValidationList", {docList:docList});
                    } else {
                        var DOIValidationListDialog = await engine.renderFile("dialog/DOIValidation", docList.pop());
                    }
                    $("#dialog").html(DOIValidationListDialog);
                    $("#dialog").dialog ({
                        width: 750,
                        title: "Validation",
                        position: {my:'center', at:'center', of: window},
                    });
                }
            }
        }

        /**
         * ajoute les DOIs des documents halids
         */
        async function addDOI(halids) {
            if(!Array.isArray(halids)) {
                halids = [halids];
            }
            for(const halid of halids) {
                await $('#'+halid+'ResultRow').replaceWith('<tr id="empty' + halid + 'ResultRow" style="height: 225px;"><tr>');
                var emptyRow = await engine.renderFile("page/result/emptyLine", {halid:halid, emptyType: 'addDOI'})
                await $('#empty' + halid + 'ResultRow').append(emptyRow);
                var DOIAddWait = await engine.renderFile('page/result/status/DOIAddWait', {
                    halid: halid,
                    DOI: halDocumentSet.getHalDoc(halid).chosenDOI,
                    url: {hal : halDocumentSet.getHalDoc(halid).haldoc['uri_s'], doi: halDocumentSet.getHalDoc(halid).selectedResult['URL']},
                });
                await $('#AddingDOI').append(DOIAddWait);
                emptyRows.push('empty' + halid);
            }
            halDocumentSet.addDOI($("#login").val(), currentPass, halids);
        }

        /**
         * ajout de document hal dans le tableau.
         */
        async function addHalDoc(halDocumentSet, haldoc) {
            var docData = {
                halid: haldoc['halId_s'],
                journalTitle_s: haldoc['journalTitle_s'],
                publicationDate_s: haldoc['publicationDate_s'],
                uri_s: haldoc['uri_s'],
                title: haldoc['title_s'][0],
                authors: haldoc['authFullName_s'],
            }
            var row = await engine.renderFile("page/result/row", docData)
            var emptyRow;
            while((emptyRow = emptyRows.pop())!=undefined) {
                if ($('#' + emptyRow + 'ResultRow').length) {
                 await $('#' + emptyRow + 'ResultRow').replaceWith(row);
                 return
                }
            }
            await $('#result-table').append(row);
        }
        $('#newSearch').hide();

        $('#newSearch').click(ev => {
            ev.preventDefault();
            $('#search-panel').show();
            if(halDocumentSet!=null) {
                halDocumentSet.abort();
            }
            $('#result').html('');
            $('#newSearch').hide();
        })
    </script>
</html>