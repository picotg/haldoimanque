<?php
namespace uga\idoine;
/**
 * 
 * Gestion des éléments ignore
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
?>

<!DOCTYPE html>
<html>
<head>
    <title>idOIne - Gestion des éléments ignore</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bm/dt-1.12.1/b-2.2.3/b-html5-2.2.3/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bm/dt-1.12.1/b-2.2.3/b-html5-2.2.3/datatables.min.js"></script>
    <link rel="icon" type="type/ico" href="favicon.ico" />
</head>
<body>
<?php require "navbar.php" ?>
<div class = "container">
    <h1 class="title">Gestion de la liste des éléments ignorés</h1><br>
    <table id="ignoreTable" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Structure</th>
                <th>Document</th>
                <th>Type</th>
            </tr>
        </thead>
        <tbody id="ignoreTableBody"></tbody>
    </table>
    <br>
    <input id="loadInput" type="file" name="loadFile" style="display: none;" />
</div>
</body>
    <script type="text/javascript">
        // liste des données dans local storage n'éatant pas des document
        const OTHER_DATA = ['portail'];
        var removedRow = [];
        var typeDict = {
            'badResult' : 'Peu pertinent',
            'byHand' : 'Ignoré manuellement',
        }
        $(document).ready(function() {
            $("#loadInput:file").change(async function (){
                var fileName = $(this).val();
                var file = $('#loadInput').prop('files')[0];
                if(file!==undefined) {
                    var content = await file.text();
                    content = JSON.parse(content);
                    for(key in content) {
                        $.ajax({
                            type:"post",
                            url:'api/ignorelist.php',
                            data: {
                                halid: key
                            },
                            success: async data => {
                                localStorage.setItem(data.halid, JSON.stringify(content[data.halid]));
                            }
                        })
                    }
                }
            });
            for( let i = 0; i < localStorage.length; i++) {
                var key = localStorage.key(i);
                if(OTHER_DATA.includes(key)) continue;
                try {
                    var JSONData = JSON.parse(localStorage.getItem(key))
                    if(JSONData.status == 'ignore') {
                        var date = new Date(JSONData.date);
                        var newLine = '<tr id="element'+ key +'"><td>' + date.toLocaleDateString() + '</td>';
                        newLine += '<td>' + JSONData.structure + '</td>';
                        newLine += '<td><a target="_blanc" href="https://hal.archives-ouvertes.fr/'+ key +'">'
                        newLine += key + '</a></td><td>';
                        newLine += typeDict[JSONData.ignoreType]
                        newLine += '</td></tr>';
                        $('#ignoreTableBody').append(newLine);
                    }
                } catch(error) {
                    OTHER_DATA.push(key);
                }
            }
            $('.remove').click(function(ev) {
                var halid = $(this).data('halid');
                $.ajax({
                    type:'post',
                    url:'api/ignorelist.php',
                    data: {
                        action:'remove',
                        halid: halid,
                    },
                    success: function(data) {
                        localStorage.removeItem(halid);
                        ignoreDataTable.row($('#element' + halid)).remove().draw();
                    }
                });
            });
            var ignoreDataTable = $('#ignoreTable').DataTable({
                dom: 'Blfrtip',
                buttons: [
                    {
                        text: 'Vider',
                        action: function ( e, dt, node, config ) {
                            for(let i = 0; i < localStorage.length; i++) {
                                var key = localStorage.key(i);
                                if(OTHER_DATA.includes(key)) continue;
                                var JSONData = JSON.parse(localStorage.getItem(key))
                                if(JSONData.status == 'ignore') {
                                    removeLocalKey = key => {
                                        return function(data) {
                                            localStorage.removeItem(key);
                                            ignoreDataTable.row($('#element' + key)).remove().draw();
                                            removedRow.push('element' + key);
                                        }
                                    }
                                    $.ajax({
                                        type: 'post',
                                        url: 'api/ignorelist.php',
                                        data: {
                                            action:'remove',
                                            halid: key,
                                        },
                                        success: removeLocalKey(key)
                                    })
                                }
                            }
                        }
                    },
                    {
                        text: 'Téléchargement',
                        action: function ( e, dt, node, config ) {
                            var content = {}
                            for( let i = 0; i < localStorage.length; i++) {
                                var key = localStorage.key(i);
                                if(OTHER_DATA.includes(key)) continue;
                                var JSONData = JSON.parse(localStorage.getItem(key))
                                if(JSONData.status == 'ignore') {
                                    content[key] = JSONData;
                                }
                            }
                            const blob = new Blob([JSON.stringify(content)], {type: "application/json"});
                            var a = $("<a style='display: none;'/>");
                            var url = window.URL.createObjectURL(blob);
                            a.attr("href", url);
                            a.attr("download", 'ingore_list.json');
                            $("body").append(a);
                            a[0].click();
                            window.URL.revokeObjectURL(url);
                            a.remove();
                        }
                    },
                    {
                        text: 'Recharger un fichier',
                        action: function ( e, dt, node, config ) {
                            $('#loadInput').trigger('click');
                        }
                    }
                ],
                fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    if(removedRow.includes(nRow.id)) {
                        ignoreDataTable.row(nRow).remove().draw();
                    }
                }
            });
        });
    </script>
</html>
