<?php

namespace uga\idoine\api;

use Exception;
use SimpleXMLElement;
use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\search\SearchField;
use uga\hallib\search\SearchQuery;
use uga\hallib\sword\Sword;
use uga\hallib\sword\TEIFile;

/**
 * API pour l'ajout le DOI d'une publication Hal.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('idoine');
session_start();

header("Content-Type: application/json");
$debug = $_SESSION['debug'] ?? false;
if($debug) {
    if(count($_SESSION['nextReturn']??[])===0) {
        $_SESSION['nextReturn'] = [['code' => 200]];
    }
}

$portail = $_POST['portail'] ?? '';

/**
 * Téléchage le fichier TEI d'une publication Hal provenant du serveur Hal.
 *
 * @param string $halid id hal (halId_s) correspondant à une publication
 * @return TEIFile TEI de la publication
 */
function getTEI(string $halid): TEIFile {
    // recherche et téléchargement d'un fichier tei dans Hal.
    $search = new SearchQuery([
        'baseQuery' => new LiteralElement([
            'value' => $halid,
            'field' => SearchField::getVarient('halId_s')
        ])
    ]);
    $search->addReturnedField(SearchField::getVarient('label_xml'));
    $getter = new OneDocQuery($search);
    $doc = $getter->getReult();
    $TEI = $doc->label_xml;

    $TEIFile = new TEIFile($TEI);
    $TEIFile->delVersionNode();
    return $TEIFile;
}


/**
 * Change ou ajoute le DOI d'une d'une publication dans Hal
 *
 * @param string $halId_s l'idhal sou form de string (champs halId_s renvoyé par l'API Hal)
 * @param string $newDOI Nouveau DOI
 * @param string $portail portail dont on a les droit
 * @param bool $debug mode debug activé
 * @return array
 */
function changeDoi(string $halId_s, string $newDOI, string $portail='', bool $debug=false): array {
    //adresse de recuperation du fichier TEI
    $urlSite = "https://hal.archives-ouvertes.fr/";

    $api = new Sword();
    $api->portail = $portail;
    $username = $_POST['username'];
    $password = $_POST['password'];
    $errorMessage = '';
    
    $originalTEI = file_get_contents($urlSite.$halId_s."/tei");
    $originalFile = getTEI($halId_s);
    $originalFile->changeDoi($newDOI);
    if(!$debug) {
        $resp = $api->sendTEI($originalFile, $username, $password, $halId_s);
        $info = $api->info;
        $http_code = $api->httpResponseCode;
        if($http_code!=200) {
            $errorMessage = 'erreur non identifier';
            try {
                $errorXML = new SimpleXMLElement($resp);
                $errorXML->registerXPathNamespace('sword', 'http://purl.org/net/sword/error/');
                $returnedNode = $errorXML->xpath("/sword:error")[0];
                if($http_code==406) {
                    if($returnedNode->summary == 'The supplied format is not the same as that identified in the Packaging header and/or that supported by the server') {
                        $errorMessage = 'TEI mal formaté';
                    }
                } elseif ($http_code==400) {
                    $verboseDescription = $errorXML->xpath("//sword:verboseDescription")[0];
                    if($verboseDescription == 'Paper Id can not be updated (erreur portail dans URL)') {
                        $errorMessage = 'Problème de droit';
                    } elseif($returnedNode->summary == 'Some parameters sent with the request were not understood') {
                        $errorMessage = '';
                        if(substr($verboseDescription, 0, strlen('{"duplicate-entry"')) == '{"duplicate-entry"') {
                            $verboseDescription = json_decode($verboseDescription, true);
                            $otherPub = array_keys($verboseDescription['duplicate-entry'])[0];
                            $errorMessage = 'DOI déjà présent pour la publication : ' . $otherPub;
                        }
                        else {
                            $errorMessage = 'paramètre incompris : ' . $verboseDescription;
                        }
                    }
                }
            } catch (Exception $e) {}
        }
    } else {
        $return = array_shift($_SESSION['nextReturn']);
        $resp = "mode debug";
        $info = "mode debug";
        $http_code = $return['code'];
        $errorMessage = $return['errorMessage'] ?? 'erreur inconnue';
        $_SESSION['ignorelist'] ??= [];
        array_push($_SESSION['ignorelist'], $_POST['halid']);
    }
    http_response_code($http_code);
    $newTEI = $urlSite.$halId_s."/tei";
    $newTEI = file_get_contents($newTEI);
    $result = [
        'info' => $info,
        'response' => $resp,
        'username' => $username,
        'doi' => $newDOI,
        'halid' => $halId_s,
        'halId_s' => $halId_s,
        'portail' => $portail,
        'url' => $api->fullURL,
        'http_code' => $http_code,
        'sendTei' => $originalFile->getValue(),
        'originalTEI' => $originalTEI,
        'newTEI' => $debug?$originalFile->getValue():$newTEI,
        'structure' => $_SESSION['structure'] ?? '',
        'debug' => $debug,
        'errorMessage' => $errorMessage,
    ];
    return $result;
}

if(isset($_POST['username'])&&isset($_POST['password'])&&isset($_POST['DOI'])&&isset($_POST['halid'])) {
    $halid = $_POST['halid'];
    echo json_encode(changeDoi($halid, $_POST['DOI'], $portail, $debug));
}
