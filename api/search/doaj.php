<?php

namespace uga\idoine\api\search;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR."vendor/autoload.php";

use uga\idoine\search\DOAJEngine;
use uga\idoine\search\Search;

/**
 * Recherche de document sur DOAJ.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

session_name('idoine');
session_start();
$debug = $_SESSION['debug'] ?? false;

if(isset($_GET['halid'])||isset($_POST['halid'])) {
    $searchEngine = new DOAJEngine($_POST['JSONData'] ?? null);
    $search = new Search($_GET['halid'] ?? $_POST['halid'], $searchEngine, $debug);
    if(isset($_GET['type'])&&$_GET['type']=='get_url') {
        echo json_encode(['url' => $search->getURL()]);
    }
    else {
        $search->executeSearch();
        http_response_code($search->getReponseCode());
        echo $search->getJsonResults();
    }
}
