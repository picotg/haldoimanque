<?php

namespace uga\idoine\api;

/**
 * 
 * API recherche dans Hal de publication sans DOI
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('idoine');
session_start();
header("Content-Type: application/json");

use ArrayObject;
use uga\idoine\search\HalSearch;

if(isset($_GET['refine'])) {
    echo HalSearch::docInfo($_GET['refine']);
    exit(0);
}

$params = (new ArrayObject($_GET))->getArrayCopy();
if(!isset($params['shownHalResult'])||!is_array($params['shownHalResult'])) {
    $params['shownHalResult'] = [];
}
if(!(isset($params['addDoctypeFilter'])&&$params['addDoctypeFilter']==='true')) {
    unset($params['doctype']);
}

if(isset($_GET['structure'])&&isset($_GET['nb_max'])&&isset($_GET['doctype'])) {
    $halSearch = new HalSearch($params);
    $_SESSION['halSearch'] = $halSearch;
} elseif (isset($_GET['shownHalResult'])&&isset($_GET['size'])) {
    $halSearch = $_SESSION['halSearch'];
    $halSearch->fromArray($params);
}

if(isset($_SESSION['ignorelist'])) {
    $halSearch->ignoreList = $_SESSION['ignorelist'];
}

echo $halSearch->data;
