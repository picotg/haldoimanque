<?php

namespace uga\idoine\api;

/**
 * API gèrant la liste des publications ignorer.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('idoine');
session_start();

$_SESSION['ignorelist'] ??= [];

header("Content-Type: application/json");

if(isset($_GET['action'])) {
    // téléchargement de la liste des documents Hal ignorer
    if (isset($_GET['action'])=='dl') {
        header('Content-Encoding: UTF-8');
        header('Content-type: application/json; charset=UTF-8');
        header('Content-disposition: attachment;filename=ignorelist.json');
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo json_encode(array_values($_SESSION['ignorelist']));
    }
}

// ajout ou suppression d'un document Hal
if(isset($_POST['halid'])) {
    // suppression
    if(isset($_POST['action'])&&$_POST['action']=='remove') {
        if(in_array($_POST['halid'], $_SESSION['ignorelist'])) {
            foreach($_SESSION['ignorelist'] as $key => $element) {
                if($element==$_POST['halid']) {
                    unset($_SESSION['ignorelist'][$key]);
                    break;
                }
            }
            echo json_encode(['halid' => $_POST['halid']]);
        } else {
            echo '{}';
        }
    // ajout
    } else {
        if(!in_array($_POST['halid'], $_SESSION['ignorelist'])) {
            array_push($_SESSION['ignorelist'], $_POST['halid']);
        }
        echo json_encode(['halid' => $_POST['halid']]);
    }
}
