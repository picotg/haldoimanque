<?php
namespace uga\idoine;
/**
 * 
 * index
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

 ?>
 <!DOCTYPE html>
<html>
<head>
    <title>idOIne</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
    <script type="text/javascript" src="js/flash_message.js"></script>
    <link rel="stylesheet" href="css/flash_message.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="type/ico" href="favicon.ico" />
</head>
<body class="content">
<?php require "navbar.php" ?>
<div class = "container">
    <h1 class="title">Affinage des recherche</h1>
    <p>Ici vous pouvez retraité les résultat ignorer pour une structure.</p>
    <div class="columns">
        <div class="column is-one-quarter">
            <div class="select">
                <select name="structure" id="structure">
                    <option>structure</option>
                </select>
            </div>
        </div>
        <div class="column">
            Nombres d'élément ignorer :
            <div class="columns">
                <div class="column">
                    Manuelement <span id="byHandShow">0</span>
                </div>
                <div class="column">
                    Mauvais résultat <span id="badResultShow">0</span>
                </div>
                <div class="column">
                    Depuis l'affinage <span id="refineShow">0</span>
                </div>
            </div>
        </div>
    </div>
    <p>
        <button class="button is-light" id="previous" disabled>précédent</button>
        <button class="button is-light is-pulled-right" id="next" disabled>suivant</button>
    </p>
    <h2 style="text-align: center;" id="idhalTitle"></h2>
    <a id="ignorCurrent" style="color: red;">
        <i class="fa fa-times" aria-hidden="true"></i>
    </a>
    <div id="showCurrent"></div>
</div>
</body>
<script>
    // moteur de rendu liquidJs
    var engine = new liquidjs.Liquid({
        root: ['views/'],
        extname: '.liquid'
    }); 
    // liste des données dans local storage n'éatant pas des document
    const OTHER_DATA = ['portail'];

    class IgnoreList {
        // type d'ignore par priorité de tratement.
        static ignoreTypeOrder = ['byHand', 'badResult', 'refine'];
        static defaultIgnoreTypes = ['byHand', 'badResult'];
        #observerCallBack = [];
        #ignoreTypes;

        constructor() {
            // liste des document ignorer classer par ignoreType
            this._list = {}
            // infos Hal et document trouver sur moteur classé par document Hal
            this._infos = {}
            this.#ignoreTypes = IgnoreList.defaultIgnoreTypes;
            for(const ignoreType of IgnoreList.ignoreTypeOrder) {
                this._list[ignoreType] = [];
            }
            this._coord = null;
        }

        addObserverCallBack(obsCallback) {
            this.#observerCallBack.push(obsCallback);
        }

        refresh() {
            for(const obsCallback of this.#observerCallBack) {
                obsCallback(this);
            }
        }

        rewind() {
            this._firstCoord = this.firstCoord;
            this._coord = this._firstCoord;
        }

        get current() {
            if(this._coord!=null) {
                var ignoreType = IgnoreList.ignoreTypeOrder[this._coord[0]];
                return this._list[ignoreType][this._coord[1]];
            } else {
                return null;
            }
        }

        get currentIgnoreType() {
            return IgnoreList.ignoreTypeOrder[this._coord[0]];
        }

        get firstCoord() {
            for(const ignoreType of this.#ignoreTypes) {
                if(this._list[ignoreType].length>0) {
                    return [this.#ignoreTypes.indexOf(ignoreType), 0];
                }
            }
        }

        get asNext() {
            var ignoreTypeCoord = this._coord[0];
            var nextSame = this._coord[1]+1;
            var ignoreType = this.#ignoreTypes[ignoreTypeCoord];
            if(this._list[ignoreType][nextSame]!==undefined) {
                return true;
            }
            while (this.#ignoreTypes[ignoreTypeCoord+1]!==undefined) {
                ignoreTypeCoord++;
                if(this._list[this.#ignoreTypes[ignoreTypeCoord]].length>0) {
                    return true;
                }
            }
            return false;
        }

        next() {
            if(this.asNext) {
                var ignoreTypeCoord = this._coord[0];
                var nextSame = this._coord[1]+1;
                var ignoreType = this.#ignoreTypes[ignoreTypeCoord];
                if(this._list[ignoreType][nextSame]!==undefined) {
                    this._coord = [ignoreTypeCoord, nextSame];
                    this.refresh();
                    return;
                }
                while (this.#ignoreTypes[ignoreTypeCoord+1]!==undefined) {
                    ignoreTypeCoord++;
                    if(this._list[this.#ignoreTypes[ignoreTypeCoord]].length>0) {
                        this._coord = [ignoreTypeCoord, 0];
                        this.refresh();
                        return;
                    }
                }
            }
        }

        get asPrevious() {
            return (this._coord[0] !== this._firstCoord[0])||(this._coord[1] !== this._firstCoord[1]);
        }

        previous() {
            if(this.asPrevious) {
                var ignoreTypeCoord = this._coord[0];
                var prevSame = this._coord[1]-1;
                var ignoreType = this.#ignoreTypes[ignoreTypeCoord];
                if(prevSame>=0) {
                    this._coord = [ignoreTypeCoord, prevSame];
                    this.refresh();
                    return;
                }
                while (this.#ignoreTypes[ignoreTypeCoord-1]!==undefined) {
                    ignoreTypeCoord--;
                    if(this._list[this.#ignoreTypes[ignoreTypeCoord]].length>0) {
                        this._coord = [ignoreTypeCoord, this._list[this.#ignoreTypes[ignoreTypeCoord]].length-1];
                        this.refresh();
                        return;
                    }
                }
            }
        }

        ignoreCurrent() {
            // edition localStorage
            var localVersion = JSON.parse(localStorage.getItem(this.current));
            localVersion.ignoreType = 'refine';
            localStorage.setItem(this.current, JSON.stringify(localVersion));

            //edition interne
            var ignoreType = this.#ignoreTypes[this._coord[0]];
            const index = this._list[ignoreType].indexOf(this.current);
            if (index > -1) {
                this._list[ignoreType].splice(index, 1);
            }
            this._list['refine'].push(this.current);
            this.refresh();
        }

        get list() {
            return this._list;
        }

        get length() {
            var total = 0
            for(const ignoreType of IgnoreList.ignoreTypeOrder) {
                total += this._list[ignoreType].length;
            }
            return total;
        }

        getCurentInfoFromHal(calback) {
            $.ajax({
                type: "get",
                url: 'api/halsearch.php',
                data: {refine: this.current},
                success: $.proxy(function(data) {
                    this._infos[this.current]['haldoc'] = data;
                    this.refresh();
                }, this)
            })
        }

        get currentHalInfo() {
            if(this._infos[this.current]['haldoc']===undefined) {
                return null;
            }
            return this._infos[this.current]['haldoc'];
        }

        lengthOf(ignoreType) {
            return this._list[ignoreType].length;
        }

        addIgnored(ignoreType, ignored) {
            if(!(ignored in this._infos)) {
                this._list[ignoreType].push(ignored);
                this._infos[ignored] = {}
            } else if(!this._list[ignoreType].includes(ignored)) {
                this._list[ignoreType].push(ignored)
                for(const ignoreType of IgnoreList.ignoreTypeOrder) {
                    const index = this._list[ignoreType].indexOf(ignored);
                    if (index > -1) {
                        this._list[ignoreType].splice(index, 1);
                    }
                }
            }
            this.refresh();
        }
    }
    // list des element ignore pour chaque type d'ignore de la structure selectionner.
    var ignored;
    // liste des structures avec élément(s) ignorer
    var structures = [];

    /**
     * (re)initialise la liste des élément ignorer
     *
     * @return void
     */
    function initIgnored() {
        ignored = new IgnoreList();
        currentCoord = null;
    }

    $(document).ready(function() {
        for( let i = 0; i < localStorage.length; i++){
            var key = localStorage.key(i);
            if(OTHER_DATA.includes(key)) continue;
            var JSONData = JSON.parse(localStorage.getItem(key))
            if(JSONData.status == 'ignore') {
                if(!structures.includes(JSONData.structure)) {
                    structures.push(JSONData.structure);
                    $('#structure').append($('<option>', {
                        value: JSONData.structure,
                        text: JSONData.structure
                    }));
                }
            }
        }
    })

    async function showCurrent(ignoreList) {
        $('#next').prop('disabled', !ignoreList.asNext);
        $('#previous').prop('disabled', !ignoreList.asPrevious);
        halid = ignoreList.current;
        $('#idhalTitle').text(halid);
        if(ignoreList.currentHalInfo == null) {
            ignoreList.getCurentInfoFromHal();
            $('#showCurrent').html('Chargement infos');
        } else {
            var currentDoc = await engine.renderFile("page/refine/currentDoc", ignoreList.currentHalInfo);
            $('#showCurrent').html(currentDoc);
        }
    }

    $('#structure').change(function() {
        if($('#structure').val()!=='structure') {
            initIgnored();
            for( let i = 0; i < localStorage.length; i++){
                var key = localStorage.key(i);
                if(OTHER_DATA.includes(key)) continue;
                var JSONData = JSON.parse(localStorage.getItem(key))
                if(JSONData.status == 'ignore'&&JSONData.structure===$('#structure').val()) {
                    ignored.addIgnored(JSONData.ignoreType, key);
                }
            }
            ignored.rewind();
            for(const ignoreType of IgnoreList.ignoreTypeOrder) {
                $('#' + ignoreType + 'Show').text(ignored.lengthOf(ignoreType));
            }
            ignored.addObserverCallBack(showCurrent);
            ignored.refresh()
        }
    });
    $('#next').click(function() {
        ignored.next();
    })
    $('#previous').click(function() {
        ignored.previous();
    })
    $('#ignorCurrent').click(function(){
        ignored.ignoreCurrent();
    })
</script>
</html>