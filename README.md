# README iDOIne

recherche de doi manquant dans Hal.

## Fonctionnalité

Identifie les publications sans DOI dans une structure de Hal. propose un DOI moissonné sur Crossref, Istex ou DOAJ et l'intègre dans la notice Hal.

## Résumer fonctionnement

Lancement d'une recherche sur Hal de documents sans DOI sur une structure. (api/halsearch.php)
Recherche d'un DOI, pour chaque document trouvé, sur les différents moteurs de recherche (api/search/crossref.php, api/search/istex.php, api/search/doaj.php).
Si non trouver recherche d'un nouveau document dans Hal. (api/halsearch.php)
possibilité d'ajouter ou ignorer un document avec un DOI trouver.
L'ajout d'un nouveau DOI sur un document se fait par l'api/DOIModifier.php.

### Modification des DOIs

L'API de modification des DOI (api/DOIModifier) utilise hallib pour modifier les DOI.

### Modification fonctionnement de recherche de DOIs

La recherche de DOIs se fait en trois temps :
1) demande de l'URL à l'API de recherche 
2) execution de la requête côté client
3) envoie du résultat à l'API de recherche
4) l'API de recherche renvoi les résultats traiter

### Minification docinfo.js

La minification de docinfo.js utilise [Uglify](https://www.npmjs.com/package/uglify-js) avec la commande :

uglifyjs --output .\js\docInfo.min.js --compress --mangle -- .\js\docInfo.js

## Test unitaire

lancement en ligne de commande :
./vendor/bin/phpunit

(⚠ En cas d'erreur SSL, ce connecter au VPN.)

## Mode débugge

en visitant l'url '/debug', on active le mode débuggage qui permet d'utiliser iDOIne sans réellement changer les donner dans Hal.

Il permet aussi de changer les code et message d'erreur pour les prochain appel à l'API DOIModifier.php.

Le mode debug est activer que pour la session en cours.

## Gestion des éléments ignorés

Les élément ignorés et leurs données sont stockés dans le LocalStorage du navigateur. La liste des documents est aussi conservée en session pour que la recherche Hal y ait directement accés. Du côté du serveur, la liste d'éléments ignorés contient aussi les erreurs.

# Fichiers et dossier

## /

- error.php : page d'historique des erreurs
- history.php : page d'historique des modifications effectuées
- ignore.php : permet de voir et modifier la liste des publications ignorées
- index.php : page principale
- navbar.php : barre pour la navigation

### api

contient les API.

- api/doctype.php : permet d'obtenir la liste des types de documents pour un portail selectionner
- api/DOIModifier.php : API pour ajouter/modifier les DOI dans Hal
- api/halsearch.php : API recherche de publications sans DOI dans Hal
- api/ignorelist.php : permet de modifier la liste des publications à ignorer

#### api/search

contient les api pour aller consulter les moteurs de recherche.

- api/search/crossref.php : api pour recherche dans crossref
- api/search/doaj.php : api pour recherche dans DOAJ
- api/search/istex.php : api pour recherche dans IsTex

### data

Contient une version de de la liste des portails ne contenant que le portail UGA. Pour utiliser cette liste, il suffit d'ajouter ?uga à l'URL

- data/instance.json : liste des portails affichés

### debug

Activation du mode debug et outil de test pour les api Hal.

- debug/index.php : permet de gérer les options de débug
- debug/generateTEI.php : télécharge le fichier TEI d'une publication en ajoutant optionnellement le DOI
- debug/tryToSend.php : envoie le fichier directement dans Hal
- debug/useDOIMod.php : envoie le fichier dans Hal en utilisant l'API DOIModifier.php

## error_exemples

exemples de résultats de l'API SWORD de Hal en cas d'erreur.

### js

- js/halRequestBuilder.js : contient le javascript pour l'autocomplétion de la recherche
- js/flash_message.js : code d'affichage de message flash
- js/docInof.js : gestion des documents Hal
- js/docInfo.min.js : version minifié de docInfo.js

minifier en utilisant : uglifyjs.cmd --output .\js\docInfo.min.js --compress --mangle -- .\js\docInfo.js

### search

Contient tous les éléments pour la recherche de DOI dans les diverses bases de données.

- search/CrossrefEngine.php : moteur de recherche dans crossref
- search/DOAJEngine.php : moteur de recherche dans DOAJ
- search/IstexEngine.php : moteur de recherche dans istex
- search/Result.php : Resultat d'une recherche sur une des divers API de recherche.
- search/Results.php : Ensemble de Resultats d'une recherche sur une des divers API de recherche.
- search/Search.php : Recherche d'un document Hal dans d'autre service
- search/SearchEngine.php : class de base pour implémenter un moteur de recherche

### test

Contient les test unitaire

#### test/hallib

Contient les test permétant de verifier la compatibilité avec la librairie hallib.

- test/hallib/HalSearchTest.php : test de l'api halsearch.php

### views

contient les templates liquidjs pour l'affichage.

#### views/dialog

- views/dialog/DOIValidation.liquid : Affiche le dialog de validation d'une modification unique
- views/dialog/DOIValidationList.liquid : Affiche le dialog de validation de plusieurs modifications
- views/dialog/searchStructure.liquid : dialog de recherche d'une structure
- views/dialog/structureTableLine.liquid : ligne de résultats dans le dialog de recherche d'une structure
- views/dialog/waitingHalResult.liquid : dialog d'attente de résultats Hal

#### views/page/result

- views/page/result/emptyLine.liquid : affichage d'une ligne vide (tableau général)
- views/page/result/launchedSearch.liquid : affichage d'attente du résultat d'une requête (tableau général)
- views/page/result/result.liquid : affiche un résultat dans la table générale des résultats (tableau général)
- views/page/result/row.liquid : affiche une ligne du tableau des recherches (tableau général)
- views/page/result/searchTable.liquide : affiche le tableau général (tableau général)

##### views/page/result/status

- views/page/result/status/errorAddDOI.liquid : affichage de l'état d'erreur d'un modification de DOI
- views/page/result/status/DOIAddWait.liquid : affichage de l'état en cours de modification de DOI
- views/page/result/status/successAddDOI.liquid : affichage de l'état DOI ajouté
