<?php
namespace uga\idoine;
$isIndex = substr($_SERVER['REQUEST_URI'], -strlen('index.php'))==='index.php' || substr($_SERVER['REQUEST_URI'], -1)==='/' || substr($_SERVER['REQUEST_URI'], -strlen('?uga'))==='?uga';
?>
<nav class="navbar" role="navigation" aria-label="main navigation">
   <div class="navbar-brand">
      <a class="navbar-item" href="index.php">
         <img src="img/logo_uga.png" height="28">
      </a>
   </div>

   <div class="navbar-menu">
      <div class="navbar-start">
         <a class="navbar-item" href="index.php">
            Accueil
         </a>
         <div class="navbar-item has-dropdown is-hoverable">
            <a <?= $isIndex?'target="_blank"':'' ?> class="navbar-link">
               Historique
            </a>
            <div id="resultLinks" class="navbar-dropdown">
               <a <?= $isIndex?'target="_blank"':'' ?> href="history.php" class="navbar-item">
                  DOI ajoutés
               </a>
               <a <?= $isIndex?'target="_blank"':'' ?> href="error.php" class="navbar-item">
                  Erreurs
               </a>
               <a <?= $isIndex?'target="_blank"':'' ?> href="ignore.php" class="navbar-item">
                  Éléments ignorés
               </a>
            </div>
         </div>
      </div>
   </div>
</nav>