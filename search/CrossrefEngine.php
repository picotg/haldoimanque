<?php

namespace uga\idoine\search;

/**
 * 
 * API recherche de DOI sur crossref
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
header("Content-Type: application/json");

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use stdClass;

/**
 * Moteur de recherche dans crossref.
 */
class CrossrefEngine extends SearchEngine {
    /**
     * {@inheritdoc}
     *
     * @var string
     */
    protected string $titleQuery = "https://api.crossref.org/works?rows=5&query.title=%%TITLE%%&select=DOI%2Ctitle%2CURL%2Cauthor%2Cpublished%2Ccontainer-title";
    /**
     * {@inheritdoc}
     *
     * @var string
     */
    protected string $authorQuery = "https://api.crossref.org/works?rows=50&query.author=%%AUTHOR%%&select=DOI%2Ctitle%2CURL%2Cauthor%2Cpublished%2Ccontainer-title";

    /**
     * {@inheritdoc}
     *
     * @param [type] $queryResults
     * @return array
     */
    public function selectResults($queryResults): array {
        if(isset($queryResults->message->items)&&is_array($queryResults->message->items)) {
            return $queryResults->message->items;
        } else {
            return [];
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param [type] $work
     * @return stdClass
     */
    public function buildWork($work): stdClass {
        $work->title = $work->title[0];
        $work->authors = [];
        if(isset($work->author)&&is_array($work->author)) {
            foreach($work->author as $author) {
                $firstName = $author->given ?? '';
                $familyName = $author->family ?? '';
                $authorFullName = $firstName.' '.$familyName;
                array_push($work->authors, $authorFullName);
            }
        }
        if(isset($work->published)) {
            $work->publicationDate = get_object_vars($work->published)['date-parts'][0][0] ?? '';
        }
        $work->journal = get_object_vars($work)['container-title'][0] ?? '';
        $work->warning = '';
        return $work;
    }
}
