<?php

namespace uga\idoine\search;

use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\DataContainer;
use uga\hallib\queryDefinition\IntervalElement;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\QueryIterator;
use uga\hallib\search\SearchField;
use uga\hallib\search\SearchQuery;

/**
 * 
 * Search Hal pour les publication sans DOI.
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * Recherche liste de document dans Hal.
 */
class HalSearch extends DataContainer {
    /**
     * liste des champs de retour de la requête Hal.
     */
    public const RETURNED_FIELD = ['title_s', 'uri_s', 'authFullName_s', 'halId_s', 'journalTitle_s', 'publicationDate_s'];

    /**
     * liste des document ignorer
     *
     * @var array
     */
    protected array $ignoreList = [];
    /**
     * Liste des document déjà afficher.
     *
     * @var array
     */
    protected array $shownHalResult = [];
    /**
     * liste des documents temporairement ignorer après ou pendant la
     * mise à jour des DOI.
     *
     * @var array
     */
    protected array $tmpIgnore = [];
    /**
     * Requêt hallib pour la requête de recherche de documents.
     *
     * @var SearchQuery
     */
    protected SearchQuery $query;
    /**
     * Document en erreurs
     *
     * @var array
     */
    protected array $error;
    /**
     * structure pour la recherche
     */
    protected string $structure;
    /**
     * Ajout de filtrre de type de documents
     *
     * @var boolean
     */
    protected bool $addDoctypeFilter = False;
    /**
     * limite inférieur de la date de production
     *
     * @var integer|null
     */
    protected ?int $producedDateStart = null;
    /**
     * limite maximale de la date de production
     *
     * @var integer|null
     */
    protected ?int $producedDateEnd = null;
    /**
     * Portail de la recherche
     *
     * @var string|null
     */
    protected ?string $portail = null;
    /**
     * Tail maximale de la liste d'élément
     */
    protected int $size = 5;
    /**
     * Tail maximale de la liste d'élément
     */
    protected int $nb_max = 5;
    /**
     * Type de documents
     *
     * @var string|null
     */
    protected ?string $doctype = null;
    /**
     * Tous le contenu d'une instance
     *
     * @var boolean
     */
    protected bool $allInstance = false;
    /**
     * à paraitre
     *
     * @var boolean
     */
    protected bool $toPub = false;

    /**
     * Recherche les infos d'un document Hal.
     *
     * @param string $halId
     * @return void
     */
    public static function docInfo(string $halId) {
        $query = new SearchQuery([
            'baseQuery' => new LiteralElement([
                'value' => $halId,
                'field' => SearchField::getVarient('halId_s'),
            ]),
        ]);
        $query->addReturnedFields(static::RETURNED_FIELD);
        return (new OneDocQuery($query))->result->json;
    }

    /**
     * Constructeur
     *
     * @param [type] $values
     */
    public function __construct($values=null) {
        parent::__construct($values);
        if(!isset($this->query)) {
            $this->_buildQuery();
        }
    }

    /**
     * Acces aux données de résultats de la recherche
     *
     * @return string
     */
    public function getData(): string {
        $queryIterator = new QueryIterator($this->query);
        $data = [];
        $data['restults'] = [];
        $accepted = 0;
        $nbIgnored = 0;
        $error = array_keys($_SESSION['errors'] ?? []);
        $skipHalResult = array_merge($this->shownHalResult, $error);
        $maxSize = $this->size - count($this->shownHalResult);
        foreach($queryIterator as $doc) {
            // ignore list
            if(in_array($doc->halId_s, $this->ignoreList)||in_array($doc->halId_s, $this->tmpIgnore)) $nbIgnored++;
            if($doc==null||in_array($doc->halId_s, $this->ignoreList)||in_array($doc->halId_s, $skipHalResult)||in_array($doc->halId_s, $this->tmpIgnore)) continue;
            array_push($data['restults'], ['haldoc' => json_decode($doc->json)]);
            $accepted++;
            if($accepted>=$maxSize) break;
        }
        $data['nbIgnored'] = $nbIgnored;
        $data['nbResult'] = $queryIterator->getNbResult();
        return json_encode($data);
    }

    /**
     * Construction d'une requête
     *
     * @return void
     */
    private function _buildQuery() {
        if($this->allInstance) {
            $baseQuery = new LiteralElement(['value' => '*']);
        } else {
            $baseQuery = new LiteralElement([
                'value' => $this->structure,
                'field' => SearchField::getVarient('structure_t'),
            ]);
        }
        $this->query = new SearchQuery([
            'baseQuery' => $baseQuery,
            'useCursor' => true,
        ]);
        if($this->allInstance && $this->portail != null) {
            $this->query->instance = $this->portail;
        }
        $this->query->addFilterQuery(new IntervalElement([
            'minValue' => '""',
            'field' => SearchField::getVarient('doiId_s'),
            'prefix' => '-',
        ]));
        if(($this->producedDateStart !== null)||($this->producedDateEnd !== null)) {
            $this->query->addFilterQuery(new IntervalElement([
                'minValue' => ($this->producedDateStart !== null)?$this->producedDateStart:'*',
                'maxValue' => ($this->producedDateEnd !== null)?$this->producedDateEnd:'*',
                'field' => SearchField::getVarient('publicationDate_s'),
            ]));
        } elseif ($this->toPub) {
            $this->query->addFilterQuery(new LiteralElement([
                'value' => true,
                'field' => SearchField::getVarient('inPress_bool'),
            ]));
        }
        if($this->doctype !== null) {
            $this->query->addFilterQuery(new LiteralElement([
                'value' => $this->doctype,
                'field' => SearchField::getVarient('docType_s'),
            ]));
        }
        $this->query->addReturnedFields(static::RETURNED_FIELD);
        if($this->portail !== null) {
            $this->query->instance  = $this->portail;
        }
    }
}
