<?php

namespace uga\idoine\search;

use stdClass;

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * 
 * Resultat d'une recherche sur une des divers API de recherche.
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Décrit un élément trouver
 */
class Result {
    /**
     * URL vers le résultat
     *
     * @var string
     */
    private string $URL;
    /**
     * Le DOI trouvé
     *
     * @var string
     */
    private string $DOI;
    /**
     * Liste des noms des auteurs
     *
     * @var array
     */
    private array $authors;
    /**
     * Titre principal trouver
     *
     * @var string
     */
    private string $title;
    /**
     * Journal de publication
     *
     * @var string
     */
    private string $journal;
    /**
     * Date de publication
     *
     * @var string
     */
    private string $publicationDate;
    /**
     * Avertissement
     *
     * @var string
     */
    private string $warning;
    /**
     * Ressemblance avec le titre de la publication Hal en pourcentage.
     *
     * @var float
     */
    private float $scorePercent = 0;

    /**
     * Constructeur
     *
     * @param stdClass $data
     */
    public function __construct(stdClass $data) {
        $this->URL = $data->URL ?? '';
        $this->DOI = $data->DOI ?? '';
        $this->authors = $data->authors ?? [];
        $this->title = $data->title ?? '';
        $this->publicationDate = $data->publicationDate ?? '';
        $this->journal = $data->journal ?? '';
        $this->warning = $data->warning ?? '';
    }

    /**
     * Accesseur pour le titre
     *
     * @return string
     */
    public function getTitle():string {
        return $this->title;
    }

    /**
     * Revoie le résultat sous forme d'une string JSON.
     *
     * @return string
     */
    public function toJSON(): string{
        return json_encode($this->toArray());
    }

    /**
     * Construi un tableau représentant le resultat.
     *
     * @return array
     */
    public function toArray(): array{
        $data = [
            'URL' => $this->URL,
            'DOI' => $this->DOI,
            'authors' => $this->authors,
            'title' => $this->title,
            'journal' => $this->journal,
            'publicationDate' => $this->publicationDate,
            'scorePercent' => $this->scorePercent,
            'warning' => $this->warning
        ];
        return $data;
    }

    /**
     * Modiffieur de $scorePercent
     *
     * @param float $scorePercent
     * @return void
     */
    public function setScorePercent(float $scorePercent) {
        $this->scorePercent = $scorePercent;
    }
}
