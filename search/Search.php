<?php

namespace uga\idoine\search;

use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\search\SearchField;
use uga\hallib\search\SearchQuery;

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * 
 * Recherche d'un document Hal dans d'autre service
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Recherche d'un document Hal dans d'autre service (CrossRef...)
 */
class Search {
    /**
     * Halid de la publie à chercher
     *
     * @var string
     */
    protected string $halid;
    /**
     * Titre de la publie à chercher
     *
     * @var array|null
     */
    private ?array $titles = null;
    /**
     * Resultat de la recherche
     *
     * @var Results
     */
    protected Results $results;
    /**
     * Moteur de recherche
     *
     * @var SearchEngine
     */
    protected SearchEngine $searchEngine;
    /**
     * Mode débuggage
     *
     * @var boolean
     */
    protected bool $debug = false;

    /**
     * Constructeur
     *
     * @param string $halid id de la publication Hal original
     * @param SearchEngine $searchEngine
     */
    public function __construct(string $halid, SearchEngine $searchEngine, bool $debug=false) {
        $this->searchEngine = $searchEngine;
        $this->halid = $halid;
        $this->debug = $debug;
        $this->results = new Results($halid);
    }

    /**
     * Return Json result
     *
     * @return string
     */
    public function getJsonResults(): string {
        $restultsArray = $this->getResults()->toArray();
        if($this->debug) {
            $restultsArray['debugInfos'] = [
                'titleSearched' => $this->getHalTitles()[0],
                'finalQuery' => $this->searchEngine->getFinalQuery(),
                'queryDirectResult' => $this->searchEngine->getQueryDirectResult(),
                'curlInfo' => $this->searchEngine->getCurlInfos(),
            ];
        }
        return json_encode($restultsArray);
    }

    /**
     * Recherche de titre à partir d'un des titres de la publication Hal.
     *
     * @param integer $index index du titre recherché
     * @return void
     */
    public function defaultSearchTitle(int $index=0): void {
        $title = $this->getHalTitles()[$index];
        $this->worksLoop($this->searchEngine->searchTitle($title));
    }

    /**
     * Recherche personnalisé d'un titre.
     *
     * @param string $title
     * @return void
     */
    public function customSearchTitle(string $title): void {
        $this->worksLoop($this->searchEngine->searchTitle($title));
    }

    /**
     * Accés à la liste des titres de la publication HAl.
     *
     * @return array liste des titres de la publication HAl
     */
    public function getHalTitles(): array {
        if($this->titles == null) {
            $halQuery = new SearchQuery([
                'baseQuery' => new LiteralElement([
                    'value' => $this->halid,
                    'field' => SearchField::getVarient('halId_s')
                ])
            ]);
            $halQuery->addReturnedField(SearchField::getVarient('title_s'));
            $this->titles = (new OneDocQuery($halQuery))->result->title_s;
        }
        return $this->titles;
    }

    /**
     * Listes des document trouver.
     *
     * @return Results
     */
    public function getResults(): Results {
        return $this->results;
    }

    /**
     * Accés au code HTTP retour de la requête
     *
     * @return int
     */
    public function getReponseCode(): int {
        return $this->searchEngine->getReponseCode();
    }

    /**
     * Transforme les retour en un objet de classe Results.
     *
     * @param array $works
     * @return void
     */
    function worksLoop(array $works) {
        foreach($works as $work) {
            $this->results->addResult(new Result($work));
        }
    }

    /**
     * Recherche par autheur
     *
     * @param string $author
     * @return void
     */
    function searchAuthor(string $author) {
        $this->worksLoop($this->searchEngine->searchAuthor($author));
    }

    /**
     * Execute les recherche en fonction des options reçut.
     * 
     * @return void
     */
    function executeSearch() {
        $this->defaultSearchTitle(0);
    }

    /**
     * Return URL for search
     *
     * @return void
     */
    function getURL() {
        return $this->searchEngine->getURLTitle($this->getHalTitles()[0]);
    }

}
