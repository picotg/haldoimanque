<?php

namespace uga\idoine\search;

/**
 * 
 * API recherche de DOI sur istex
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
header("Content-Type: application/json");

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use stdClass;

/**
 * Moteur de recherche dans IsTex.
 */
class IstexEngine extends SearchEngine {
    /**
     * {@inheritdoc}
     *
     * @var string
     */
    protected string $titleQuery = "https://api.istex.fr/document/?q=title:%%TITLE%%&output=title,doi,author,publicationDate,host";
    /**
     * {@inheritdoc}
     *
     * @var string
     */
    protected string $authorQuery = "https://api.istex.fr/document/?q=author:%%AUTHOR%%&output=title,doi,author,publicationDate,host";

    /**
     * {@inheritdoc}
     *
     * @param [type] $queryResults
     * @return array
     */
    public function selectResults($queryResults): array {
        if(isset($queryResults->hits)&&is_array($queryResults->hits)) {
            return $queryResults->hits;
        } else {
            return [];
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param [type] $work
     * @return stdClass
     */
    public function buildWork($work): stdClass {
        $warning='';
        if(isset($work->doi)) {
            if (count($work->doi)>1) $warning = 'le resultat retourne plusieurs DOI';
            $work->DOI = $work->doi[0];
            $work->URL = 'https://doi.org/' . $work->DOI;
        }
        $work->authors = [];
        if(isset($work->author)&&is_array($work->author)) {
            foreach($work->author as $author) {
                $authorFullName = $author->name;
                array_push($work->authors, $authorFullName);
            }
        }
        $work->journal = $work->host->title;
        $work->warning = $warning;
        return $work;
    }
}
