<?php

namespace uga\idoine\search;

/**
 * 
 * API recherche de DOI sur DOAJ
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
header("Content-Type: application/json");

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use stdClass;

/**
 * Moteur de recherche dans DOAJ.
 */
class DOAJEngine extends SearchEngine {
    /**
     * {@inheritdoc}
     *
     * @var string
     */
    protected string $titleQuery = "https://doaj.org/api/search/articles/title%3A%%TITLE%%";
    /**
     * {@inheritdoc}
     *
     * @var string
     */
    protected string $authorQuery = "https://doaj.org/api/search/articles/author%253A%22%%AUTHOR%%%22";

    /**
     * {@inheritdoc}
     *
     * @param [type] $queryResults
     * @return array
     */
    public function selectResults($queryResults): array {
        if(isset($queryResults->results)&&is_array($queryResults->results)) {
            return $queryResults->results;
        } else {
            return [];
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param [type] $work
     * @return stdClass
     */
    public function buildWork($work): stdClass {
        $work = $work->bibjson;
        $work->authors = [];
        foreach($work->identifier as $indetifier) {
            if($indetifier->type == 'doi') {
                $work->DOI = $indetifier->id;
            }
        }
        if(isset($work->link[0]->url)) {
            $work->URL = $work->link[0]->url;
        } else {
            $work->URL = 'https://doi.org/' . $work->DOI;
        }
        if(isset($work->author)&&is_array($work->author)) {
            foreach($work->author as $author) {
                $authorFullName = $author->name;
                array_push($work->authors, $authorFullName);
            }
        }
        $work->journal = $work->journal->title;
        $work->publicationDate = $work->year;
        $work->warning = '';
        return $work;
    }
}
