<?php

namespace uga\idoine\search;

use stdClass;

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * 
 * Ensemble de Resultats d'une recherche sur une des divers API de recherche.
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Décrit les résultats d'une recherche
 */
class Results {
    /**
     * Document Hal objet de la recherche
     *
     * @var string
     */
    private string $halid;
    /**
     * Donnée extraite de Hal au sujet du document chercher
     *
     * @var stdClass|null
     */
    private ?stdClass $halData = null;
    /**
     * Liste des résultats trouver
     *
     * @var array
     */
    private array $resultList = [];
    /**
     * Meilleur score
     *
     * @var float
     */
    private float $bestScore = 0;
    /**
     * Meilleur résultat trouver
     *
     * @var Result|null
     */
    private ?Result $bestResult = null;

    /**
     * Meilleur score résultat en poursentage
     *
     * @var integer
     */
    private int $bestScorePersent = 0;
    
    /**
     * Charge les donnée à partir de l'API Hal
     * 
     * @param string $fl liste des champs cherché dans l'API HAl.
     * @return stdClass
     */
    public function getHalData(string $fl='title_s,journalTitle_s'): stdClass {
        if($this->halData == null) {
            $halQuery="https://api.archives-ouvertes.fr/search/?q=halId_s%3A".$this->halid."&fl=".$fl;
            $this->halData = json_decode(file_get_contents($halQuery))->response->docs[0];
        }
        return $this->halData;
    }

    /**
     * Accés au titres Hal
     *
     * @return array
     */
    public function getHalTitles():array {
        return $this->getHalData()->title_s;
    }

    /**
     * Constructeur
     *
     * @param string $halid
     */
    public function __construct(string $halid)
    {
        $this->halid = $halid;
    }

    /**
     * Ajouté un résultat
     *
     * @param Result $newResult
     * @return void
     */
    public function addResult(Result $newResult) {
        array_push($this->resultList, $newResult);
        $percent = 0;
        $score = similar_text(strtolower($this->getHalTitles()[0]), strtolower($newResult->getTitle()), $percent);
        $newResult->setScorePercent($percent);
        if($score > $this->bestScore||$this->bestScore==0) {
            $this->bestResult = $newResult;
            $this->bestScore = $score;
        }
    }

    /**
     * Construit une représentaion sous forme de tableau.
     *
     * @return array
     */
    public function toArray(): array{
        $data = [
            'bestScorePercent' => $this->bestScorePersent ?? 0,
            'resultList' => [],
        ];
        if($this->bestResult != null) {
            $data['bestResult'] = $this->bestResult->toArray();
        }
        foreach($this->resultList as $result){
            array_push($data['resultList'], $result->toArray());
        }
        return $data;
    }

    /**
     * Construit une représentaion au format JSON
     *
     * @return string
     */
    public function toJSON(): string {
        return json_encode($this->toArray());
    }
}