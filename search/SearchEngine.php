<?php

namespace uga\idoine\search;

/**
 * 
 * API recherche de DOI sur crossref
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

use stdClass;

/**
 * décrit un moteur de recherche
 */
abstract class SearchEngine {
    /**
     * curlHandle pour envoyer les requête
     */
    private $curlHandle;
    /**
     * URL d'appel de l'API pour une recherche par titre
     *
     * @var string
     */
    protected string $titleQuery;
    /**
     * URL d'appel de l'API pour une recherche par auteur
     *
     * @var string
     */
    protected string $authorQuery;
    /**
     * Dernière requête executer
     *
     * @var string
     */
    protected string $finalQuery;
    /**
     * Résultat de la dernière requête executer
     *
     * @var string
     */
    protected string $queryDirectResult;
    /**
     * DataJSON si la requête est executer côté client
     *
     * @var string
     */
    protected ?string $JSONData;

    /**
     * Acces à toute les info curl au format JSON.
     *
     * @return string
     */
    public function getCurlInfos(): string {
        return json_encode(curl_getinfo($this->curlHandle));
    }

    /**
     * Acces au résultat de la requête.
     *
     * @return void
     */
    public function getQueryDirectResult() {
        return $this->queryDirectResult;
    }

    /**
     * finalQuery getter
     *
     * @return string
     */
    public function getFinalQuery(): string {
        return $this->finalQuery;
    }

    /**
     * Nétoie une chaine de caractère des caractère génant pour la recherche
     *
     * @param string $text
     * @return string
     */
    public function clean(string $text): string {
        $text = str_replace('&', '%26', $text);
        $text = str_replace('(', '%28', $text);
        $text = str_replace(')', '%29', $text);
        return urlencode($text);
    }

    /**
     * Constructeur
     */
    public function __construct($JSONData=null) {
        $this->JSONData = $JSONData;
        $this->curlHandle = curl_init();
        curl_setopt($this->curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlHandle, CURLOPT_CUSTOMREQUEST, "GET");
    }

    /**
     * destructeur
     */
    public function __destruct() {
        curl_close($this->curlHandle);
    }

    /**
     * Renvoi le code erreur de la dernière requête utilisant curl.
     *
     * @return int HTTP code retour
     */
    public function getReponseCode(): int {
        return curl_getinfo($this->curlHandle, CURLINFO_HTTP_CODE);
    }

    /**
     * execute une requête et renvoie le résultat.
     *
     * @param string $query
     * @return stdClass
     */
    public function getQueryResult(string $query): stdClass {
        if($this->JSONData != null) {
            $queryResult = json_decode($this->queryDirectResult = $this->JSONData);
            if($queryResult==null) $queryResult = new stdClass();
            return $queryResult;
        }
        curl_setopt($this->curlHandle, CURLOPT_URL, $query);
        $this->queryDirectResult = curl_exec($this->curlHandle);
        $queryResult = json_decode($this->queryDirectResult);
        if($queryResult==null) $queryResult = new stdClass();
        return $queryResult;
    }

    /**
     * Recherche un titre.
     *
     * @param string $title
     * @return array
     */
    function searchTitle(string $title): array {
        $this->finalQuery = str_replace("%%TITLE%%", $this->clean($title), $this->titleQuery);
        $queryResults = $this->getQueryResult($this->finalQuery);
        $works = [];
        foreach($this->selectResults($queryResults) as $work) {
            $newWork = $this->buildWork($work);
            if(isset($newWork->DOI)) {
                array_push($works, $newWork);
            }
        }
        return $works;
    }

    /**
     * Return url research for title.
     *
     * @param string $title
     * @return string
     */
    function getURLTitle(string $title): string {
        return str_replace("%%TITLE%%", $this->clean($title), $this->titleQuery);
    }

    /**
     * Recherche un auteur
     *
     * @param string $author
     * @return array
     */
    function searchAuthor(string $author): array {
        $query = str_replace("%%AUTHOR%%", $this->clean($author), $this->authorQuery);
        $queryResults = $this->getQueryResult($query);
        $works = [];
        foreach($this->selectResults($queryResults) as $work) {
            array_push($works, $this->buildWork($work));
        }
        return $works;
    }

    /**
     * Return url research for author.
     *
     * @param string $author
     * @return string
     */
    function getURLAuthor(string $author): string {
        return str_replace("%%AUTHOR%%", $this->clean($author), $this->authorQuery);
    }

    /**
     * Permet de trouver le tableau des résultat parmit le résultat renvoyé par la requête
     *
     * @param stdClass $queryResults
     * @return array
     */
    public abstract function selectResults(stdClass $queryResults): array;

    /**
     * Permet de formaté les champs des résultat au format attendu par l'objet result.
     *
     * @param stdClass $uncompliteWork
     * @return stdClass
     */
    public abstract function buildWork(stdClass $uncompliteWork): stdClass;
}