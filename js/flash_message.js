function show_flash_messages(message, type='info') {
    $('#flash_messages').append(`<div class="notification is-${type}"><button class="delete"></button>${message}</div>`)
}

function update_flash_message() {
    (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
        const $notification = $delete.parentNode;
        $delete.addEventListener('click', () => {
            $notification.parentNode.removeChild($notification);
        });
        setTimeout(function () {
            $('#flash_messages .notification:last').last().remove()
        }, 5000)
    });
}

function init_flash_messages_observer() {
    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

    var observer = new MutationObserver(function (mutations, observer) {
        // declenche quand mutation
        update_flash_message()
    });

    // on définit le noeud a surveiller et le type de mutation a surveiller
    observer.observe(document.getElementById('flash_messages'), {
        childList: true
    });
}
