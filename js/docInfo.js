/**
 * gestion des infos d'un document Hal et des élément trouver
 */

/**
 * @callback HalDocument~observerCallBack
 * @param {HalDocument} document document éméteur du nouveau status
 */

/**
 * Document Hal
 */
class HalDocument {
    // id Hal du document
    #halid;
    // status du document
    #status = {status: 'wait'};
    // liste des observer à notifier en cas de modification
    #observerList = {'all': []};
    // DOI courrament choisi
    #chosenDOI = '';
    // opération d'Ajout de DOI
    #addDOIRequest = null;
    // list de moteur à utiliser
    #engines = ['crossref', 'istex', 'doaj'];
    // list des nom affichable des moteurs
    #engineNames = {
        'crossref': 'Crossref',
        'istex': 'Istex',
        'doaj': 'DOAJ'
    };
    // requête courrent de recherche
    #searchQuery = null;
    // resultat des recherche sur les moteur extérieur
    #results = [];
    // meilleur résultat trouver
    #selectedResult = null;
    // moteur de recherche courant
    #currentEngine = 'crossref';
    // nom structure rechercher
    #structure = '';
    // document hal
    #haldoc = {}
    // Set de document Hal si non null (HalDocumentSet)
    #docSet = null
    // use JSON data
    #useGetJSONData = true
    // autoNext
    #autoNext = true

    /**
     * Set de document Hal (HalDocumentSet)
     * @type {?HalDocumentSet}
     */
    get docSet() {
        return this.#docSet;
    }

    /**
     * change la liste complète des observer pour le document
     * @type {HalDocument~observerCallBack[]}
     */
    set observerList(observerList) {
        if(!('all' in observerList)) {
            observerList['all'] = [];
        }
        this.#observerList = observerList;
    }

    /**
     * statut du document
     * 
     * valeur possible :
     * - wait : attente
     * - search : Recherche un DOI sur un moteur
     * - adding : en cours d'ajout de DOI
     * - added : DOI ajouter
     * - error : essai ajout DOI et erreur
     * - error401 : erreur de login/mot de passe
     * - abort : ajout arrêter
     * - ignore : ajouter à la liste d'ignore
     * - ignoreError : erreur dans l'ajout à la liste d'ignore
     * - findedDOI : un document avec DOI a était trouver
     * 
     * @type {string}
     */
    get status() {
        return this.#status;
    }

    /**
     * DOI choisi courant
     * @type {string}
     */
    get chosenDOI() {
        return this.#chosenDOI;
    }

    /**
     * moteur de recherche courant
     * @type {string}
     */
    get currentEngine() {
        return this.#currentEngine;
    }

    /**
     * liste des résultats
     * @type {Object[]}
     */
    get results() {
        return this.#results;
    }

    /**
     * document Hal détailler
     * @type {Object}
     */
    get haldoc() {
        return this.#haldoc;
    }

    /**
     * structure
     * @type {string}
     */
    get structure() {
        return this.#structure;
    }

    /**
     * retourne le nom du moteur de recherche courant
     * @type {string}
     */
    get currentEngineName() {
        return this.#engineNames[this.#currentEngine];
    }

    /**
     * liste des moteurs de recherche
     * @type {string[]}
     */
    get engines() {
        return this.#engines;
    }

    /**
     * résultat séléctionner
     * @type {Object}
     */
    get selectedResult() {
        return this.#selectedResult;
    }

    set selectedResult(selectedResult) {
        this.#chosenDOI = selectedResult['DOI'];
        this.#selectedResult = selectedResult;
    }

    /**
     * Obtenir le prochain moteur de recherche
     * @param {string} searchEngine moteur de recherche courrent
     * @returns {string} prochain moteur de recherche
     */
    getNextEngine(searchEngine) {
        var next = false;
        for(const currentEngine of this.#engines) {
            if(next) {
                return currentEngine
            }
            if(currentEngine===searchEngine) {
                next = true;
            }
        }
        return '';
    }

    /**
     * constructeur
     * @param {Object} haldoc document Hal renvoyer par la recherche
     * @param {string} structure structure rechercher dans Hal
     * @param {?HalDocumentSet} docSet set de document
     */
    constructor(haldoc, structure, docSet=null) {
        this.#halid = haldoc['halId_s'];
        this.#haldoc = haldoc;
        this.#structure = structure;
        this.#docSet = docSet;
    }

    /**
     * ajout un observateur pour un ou tous les status
     * @param {HalDocument~observerCallBack} observer observer à ajouter
     * @param {string} status status à observer (all = tous)
     */
    addObserver(observer, status='all') {
        if(this.#observerList[status]===undefined) {
            this.#observerList[status] = [];
        }
        this.#observerList[status].push(observer);
    }

    /**
     * execute la liste des observateur pour le status courant et pour tous les status.
     */
    async refresh() {
        for(const observer of this.#observerList['all']) {
            await observer(this);
        }
        if(this.#status['status'] in this.#observerList) {
            for(const observer of this.#observerList[this.#status['status']]) {
                await observer(this);
            }
        }
    }

    /**
     * Choisi un DOI (automatiquement appeler si on choisi un nouveau document selectionner)
     * @param {string} newDOI nouveau DOI
     */
    chooseDOI(newDOI) {
        this.#chosenDOI = newDOI;
    }

    /**
     * Annule les requête en cours (recherche comme ajout de DOI).
     */
    abort() {
        if(this.#addDOIRequest != null) {
            this.#addDOIRequest.abort();
        }
        if(this.#searchQuery != null && typeof this.#searchQuery.abort === 'function') {
            this.#searchQuery.abort();
        }
    }

    /**
     * le résultat est-il mauvais (oui, si null ou inferieur à 75%)
     * @type {boolean}
     */
    get isBadResult() {
        return this.#selectedResult !== null && this.#selectedResult['scorePercent'] <= 75;
    }

    /**
     * ignore le document
     * @param {stirng} ignoreType type d'ignore (manuel, mauvais résultat,...)
     */
    async ignore(ignoreType='') {
        if(ignoreType==='') {
            if(this.isBadResult) {
                ignoreType='badResult';
            }
            else {
                ignoreType='byHand';
            }
        }
        if(this.#searchQuery != null && typeof this.#searchQuery.abort === 'function') {
            this.#searchQuery.abort();
        }
        this.#status = {
            status: 'ignore',
            structure: this.#structure,
            date: Date.now(),
            ignoreType: ignoreType,
        }
        this.saveToLocalstorage();
        await this.refresh();
        $.ajax({
            type:"post",
            url:'api/ignorelist.php',
            data: {
                halid: this.#halid,
            },
            error: $.proxy(async function() {
                this.#status = {status: 'ignoreError'};
                await this.refresh();
            }, this)
        })
    }

    /**
     * ajoute le DOI choisi au document dans Hal
     * @param {string} usernam login Hal
     * @param {string} password mot de passe Hal
     * @param {stirng} portail portail Hal
     */
    addDOI(usernam, password, portail) {
        if(this.#chosenDOI != '') {
            this.abort();
            this.#status = {status: 'adding'};
            this.#addDOIRequest = $.ajax({
                type:"post",
                url: 'api/DOIModifier.php',
                data: {
                    portail: portail,
                    password: password,
                    username: usernam,
                    DOI: this.#chosenDOI,
                    halid: this.#halid,
                },
                success: $.proxy(async function(data) {
                    this.#status = {
                        DOI: this.#chosenDOI,
                        structure: data['structure'],
                        portail: data['portail'],
                        status: 'added',
                        date: Date.now(),
                    }
                    this.saveToLocalstorage();
                    await this.refresh();
                }, this),
                statusCode: {
                    401: $.proxy(async function() {
                        this.#status = {status: 'error401'};
                        await this.refresh();
                    }, this)
                },
                error: $.proxy(async function(request, textStatus) {
                    if(request.status!=401&&request.statusText!='abort') {
                        this.#status = {
                            DOI: this.#chosenDOI,
                            status: 'error',
                            response: request.responseText,
                            errorCode: request.status,
                            structure: this.#structure,
                            errorMessage: JSON.parse(request.responseText).errorMessage,
                            date: Date.now(),
                        };
                        $.ajax({
                            type:"post",
                            url:'api/ignorelist.php',
                            data: {
                                halid: this.#halid,
                            },
                        })
                        this.saveToLocalstorage();
                    } else if(request.status!=401) {
                        this.#status = {status: 'abort'};
                    }
                    await this.refresh();
                }, this)
            })
        }
    }

    /**
     * sauvegarde le status du document dans le localstorage
     */
    saveToLocalstorage() {
        localStorage.setItem(this.#halid, JSON.stringify(this.#status));
    }

    /**
     * id Hal (Halid) du document
     * @type {string}
     */
    get halid() {
        return this.#halid;
    }

    async addResult(data) {
        this.#results = this.#results.concat(data['resultList']);
        if(data['bestResult']!=undefined||this.#autoNext) {
            if(data['bestResult']!=undefined&&(this.#selectedResult==null||this.#selectedResult['scorePercent']<=data['bestResult']['scorePercent'])) {
                this.selectedResult = data['bestResult'];
            }
            if(this.#autoNext&&(data['bestResult']==undefined||data['bestResult']['scorePercent']<=75)) {
                var nextEngine = this.getNextEngine(this.#currentEngine);
                if(nextEngine!='') {
                    this.#currentEngine = nextEngine;
                    this.searchDOI();
                } else {
                    this.#status = {status: 'wait'};
                    await this.refresh();
                }
            } else {
                this.#status = {status: 'wait'};
                await this.refresh();
            }
        } else {
            this.#status = {status: 'wait'};
            await this.refresh();
        }
    }

    async searchWithJSONData(JSONData) {
        this.#searchQuery = $.ajax({
            type:"post",
            url: 'api/search/'+this.#currentEngine+'.php',
            data: {
                halid: this.#halid,
                JSONData: JSON.stringify(JSONData)
            },
            success: $.proxy(this.addResult, this),
            error: $.proxy(async function(request, testStatus){
                if (request.statusText != 'abort') {
                    this.#status = {status: 'wait'};
                    await this.refresh();
                    if(this.#autoNext) {
                        var nextEngine = this.getNextEngine(this.#currentEngine)
                        if(nextEngine!='') {
                            this.#currentEngine = nextEngine;
                            this.searchDOI();
                        }
                    }
                }
            }, this)
        })
    }

    async getJSONData(data) {
        this.#searchQuery = $.ajax({
            type:"get",
            url: data.url
        });
        this.#searchQuery.done($.proxy(this.searchWithJSONData, this))
    }

    async getSearchURL() {
        this.#searchQuery = await $.ajax({
            type:"get",
            url: 'api/search/'+this.#currentEngine+'.php',
            data: {
                halid: this.#halid,
                type: 'get_url'
            },
            success: $.proxy(this.getJSONData, this)
        })
    }

    /**
     * Recherche le DOI sur le moteur de recherche courant
     * @param {boolean} autoNext recherche automatiquement avec le moteur de recherche suivant
     */
    async searchDOI(autoNext=true) {
        this.#status = {status: 'search'};
        await this.refresh();
        if(this.#useGetJSONData) {
            await this.getSearchURL();
        } else {
            this.#searchQuery = $.ajax({
                type:"get",
                url: 'api/search/'+this.#currentEngine+'.php',
                data: {
                    halid: this.#halid,
                },
                success: $.proxy(this.addResult, this),
                error: $.proxy(async function(request, testStatus){
                    if (request.statusText != 'abort') {
                        this.#status = {status: 'wait'};
                        await this.refresh();
                        if(this.#autoNext) {
                            var nextEngine = this.getNextEngine(this.#currentEngine)
                            if(nextEngine!='') {
                                this.#currentEngine = nextEngine;
                                this.searchDOI();
                            }
                        }
                    }
                }, this)
            })
        }
    }
}

/**
 * @callback HalDocumentSet~observerCallBack
 * @param {HalDocumentSet} documents set de documents
 * @param datas datas envoyer au callback
 */

/**
 * Ensemble de documents Hal
 */
class HalDocumentSet{
    // liste des document hal indexé par leur halid
    #halDocuments = {};
    // liste des document hal visible
    #shownHalResult = [];
    // requête pour complété la liste des documents
    #completeRowQuery = null;
    // liste des documents [halid] en cours d'ajout de DOI
    #waitingDOIadd = [];
    // nombre d'ajouts de DOI
    #nbAdd = 0;
    // list des publication ignorer temporairement
    #tmpIgnore = [];
    // list de TODO à 0
    #allDone = false;
    // list d'observer indexer par event observer
    #observers = {};
    // autres paramétres de la requête
    #queryParameter = {}
    // observer à ajouter au nouveau document
    #documentObservers = {}
    // observer pour l'instance courante
    #selfObserver
    // liste temporaire de DOI à ajouter pour pause
    #tmpAddDOI = []

    /**
     * getter nombre de document ajouter
     * @type {number}
     */
    get nbAdd() {
        return this.#nbAdd;
    }

    /**
     * incrémente le nombre de DOI ajouté depuis le début de la recherche
     */
    incrementNbAdd() {
        this.#nbAdd++;
    }

    /**
     * supprime ledocument halid de la liste des documents en cours d'ajout de DOI
     * @param {string} halid id Hal
     */
    removeWaitingDOIAdd(halid) {
        this.#waitingDOIadd.splice(this.#waitingDOIadd.indexOf(halid), 1);
    }

    /**
     * prise en compte d'un ajout de DOI.
     * @param {HalDocument} halDocument document éméteur de l'événement
     */
    async DOIAdded(halDocument) {
        halDocument.docSet.incrementNbAdd();
        halDocument.docSet.removeWaitingDOIAdd(halDocument.halid);
        await halDocument.docSet.notify('nbAddChange', halDocument.docSet.nbAdd);
    }

    /**
     * prise en compte d'une erreur durant l'ajout de DOI.
     * @param {HalDocument} halDocument document éméteur de l'événement
     */
    async addDOIError(halDocument) {
        halDocument.docSet.removeWaitingDOIAdd(halDocument.halid);
        halDocument.docSet.completeRow();
    }

    /**
     * suppression du document halid de la liste des document affiché.
     * @param {string} halid id Hal
     */
    removeShawnHalDocument(halid) {
        if(this.#shownHalResult.includes(halid)) {
            this.#shownHalResult.splice(this.#shownHalResult.indexOf(halid), 1);
        }
    }

    /**
     * constructeur
     * @param {Object} queryParameter paramétre de la requête
     */
    constructor(queryParameter) {
        this.#queryParameter = queryParameter;
        this.#selfObserver = {
            'added': this.DOIAdded,
            'error': this.addDOIError,
        }
    }

    /**
     * annulation des oppération courante.
     */
    abort() {
        for(const halid in this.#halDocuments) {
            this.#halDocuments[halid].abort();
        }
        if(this.#completeRowQuery != null) {
            this.#completeRowQuery.abort();
        }
    }

    /**
     * ignore une liste de document.
     * @param {string|string[]} halids ids Hal des document à ignorer
     * @param {string} ignoreType type d'ignorance (manuelle, résultat peut pertinant)
     */
    async ignore(halids, ignoreType='') {
        if(!Array.isArray(halids)) {
            halids = [halids];
        }
        for(const halid of halids) {
            await this.#halDocuments[halid].ignore(ignoreType);
            this.removeShawnHalDocument(halid)
        }
        this.completeRow();
    }

    /**
     * recherche les premiers documents dans Hal.
     */
    async searchHalDocument() {
        var data = {
            nb_max: this.#queryParameter['nbMax'],
            size: this.#queryParameter['nbMax'],
            structure: this.#queryParameter['structure'],
            doctype: this.#queryParameter["doctype"],
            addDoctypeFilter: this.#queryParameter["addDoctypeFilter"],
            portail: this.#queryParameter['portail'],
            toPub: this.#queryParameter['toPub'],
            allInstance: this.#queryParameter['allInstance'],
        }
        if(this.#queryParameter["producedDateInterval"]) {
            data.producedDateStart = this.#queryParameter["producedDateStart"];
            data.producedDateEnd = this.#queryParameter["producedDateEnd"];
        }
        await $.ajax({
            type: "get",
            url: 'api/halsearch.php',
            data: data,
            success: $.proxy(async function(data) {
                await this.notify('searchFinish', data)
                data.restults.forEach(async haldoc => {
                    await this.addHalDoc(haldoc);
                });
            }, this),
            error: $.proxy(async function() {
                await this.notify('HalSearchError')
            }, this)
        })
    }

    /**
     * accé pour le document halid.
     * @param {string} halid id Hal du document voulue
     * @returns {HalDocument} document correspondant à l'id Hal
     */
    getHalDoc(halid) {
        return this.#halDocuments[halid];
    }

    /**
     * Ajoute un nouveau document.
     * @param {Object} haldoc document Hal retourner par la recherche
     */
    async addHalDoc(haldoc) {
        var halid = haldoc['haldoc']['halId_s'];
        if(!(halid in this.#halDocuments)) {
            var newHalDoc = new HalDocument(haldoc['haldoc'], this.#queryParameter['structure'], this);
            newHalDoc.observerList = this.#documentObservers;
            this.#shownHalResult.push(halid);
            this.#halDocuments[halid] = newHalDoc;
            await this.notify('addHalDoc', haldoc['haldoc']);
            newHalDoc.searchDOI();
        } else {
            this.completeRow();
        }

    }

    /**
     * compléte la liste des documents en allant chercher dans Hal.
     */
    async completeRow() {
        if(this.#shownHalResult.length >= this.#queryParameter['nbMax']) {
            return;
        }
        if(this.#completeRowQuery!=null) {
            this.#completeRowQuery.abort();
        }
        this.#completeRowQuery = $.ajax({
            type: "get",
            url: 'api/halsearch.php',
            data: {
                shownHalResult: (this.#shownHalResult.length!=0)?this.#shownHalResult:'',
                size: this.#queryParameter['nbMax'],
                tmpIgnore: this.#tmpIgnore,
            },
            success: $.proxy(async function(data) {
                await this.notify('completeRowFinish', data);
                data.restults.forEach(async haldoc => {
                    await this.addHalDoc(haldoc);
                });
                if(data.nbResult-data.nbIgnored-this.#shownHalResult.length <= 0) {
                    this.#allDone = true;
                    await this.notify('allDone', true);
                }
            }, this),
            error: $.proxy(async function(request, testStatus) {
                if (request.statusText != 'abort') {
                    await this.notify('errorCompleteRow');
                }
            }, this)
        })
    }

    /**
     * liste des observers pour les documents
     * @type {HalDocument~observerCallBack[]}
     */
    set documentObservers(documentObservers) {
        for(const status in this.#selfObserver) {
            if(documentObservers[status] === undefined) {
                documentObservers[status] = []
            }
            documentObservers[status].push(this.#selfObserver[status]);
        }
        this.#documentObservers = documentObservers;
    }

    /**
     * notify les observer de l'événement event.
     * @param {string} event nom de l'événement
     * @param {} datas donnée à envoyer
     */
    async notify(event='', datas='') {
        if(event in this.#observers) {
            for(const observer of this.#observers[event]) {
                await observer(this, datas);
            }
        }
    }

    /**
     * ajoute un observer pour un événement.
     * @param {HalDocumentSet~observerCallBack} observer observer à ajouter
     * @param {string} event événement observer
     */
    addObserver(observer, event='') {
        if(!(event in this.#observers)) {
            this.#observers[event] = []
        }
        this.#observers[event].push(observer);
    }

    /**
     * Tous les document de la requête on été traité
     * @type {boolean}
     */
    get allDone() {
        return this.#allDone;
    }

    /**
     * mise en pause des ajout de DOI.
     */
    pauseAddDOI() {
        for(const halid of this.#waitingDOIadd) {
            this.#halDocuments[halid].abort();
            this.#tmpAddDOI.push(halid)
        }
        this.#waitingDOIadd = [];
    }

    /**
     * réenvoie les ajouts DOI mis en pause
     * @param {string} login login Hal
     * @param {string} password mot de passe Hal
     */
    resendDOI(login, password) {
        for(const halid of this.#tmpAddDOI) {
            this.addDOI(login, password, halid);
        }
    }
    
    /**
     * Ajout DOI.
     * @param {string} login login Hal
     * @param {string} password mot de passe Hal
     * @param {string|string[]} halids liste des id Hal des documents dont il faut ajouter le DOI
     */
    addDOI(login, password, halids) {
        if(!Array.isArray(halids)) {
            halids = [halids];
        }
        for(const halid of halids) {
            if(this.#halDocuments[halid].chosenDOI != '') {
                if(!this.#tmpIgnore.includes(halid)) {
                    this.#tmpIgnore.push(halid);
                }
                this.removeShawnHalDocument(halid);
                this.#waitingDOIadd.push(halid);
                this.#halDocuments[halid].addDOI(login, password, this.#queryParameter['portail']);
            }
        }
        this.completeRow();
    }
}
