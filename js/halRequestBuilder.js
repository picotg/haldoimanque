$(document).ready(function(){
    $("#structure").autocomplete({
        source: (request, response) => {
        $.ajax({
            url: 'https://api.archives-ouvertes.fr/ref/structure',
            data: {
                q: 'text_autocomplete:'+request.term,
                fl: 'label_s,acronym_s'
            },
            success: data => {
                respData = [];
                idIn = [];
                data['response']['docs'].forEach(dataElement => {
                    newId = dataElement['acronym_s'];
                    if(!idIn.includes(newId)) {
                    respData.push({
                        label: dataElement['label_s'],
                        id: newId,
                    });
                    idIn.push(newId);
                    }
                })
                response(respData);
            },
        });
        },
        minLength: 2,
        select: (event, ui) => {
        $('#structureHidden').val(ui.item.id);
        }
    });
})