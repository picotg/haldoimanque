<?php
namespace uga\idoine;
/**
 * 
 * Historique des modification de DOI
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title>idOIne - Historique</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bm/dt-1.12.1/b-2.2.3/b-html5-2.2.3/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bm/dt-1.12.1/b-2.2.3/b-html5-2.2.3/datatables.min.js"></script>
    <link rel="icon" type="type/ico" href="favicon.ico" />
</head>
<body>
<?php require "navbar.php" ?>
<div class = "container">
    <h1 class="title">Historique des ajouts de DOI</h1>
    <br>
    <table id="addHistoy" style="width:100%">
        <thead>
            <tr>
                <th>Structure</th>
                <th>Date</th>
                <th>Document Hal</th>
                <th>DOI</th>
            </tr>
        </thead>
        <tbody id="addedTableBody">
        </tbody>
    </table>
</div>
</body>
<script>
    // liste des données dans local storage n'éatant pas des document
    const OTHER_DATA = ['portail'];

    function downData(data, name) {
        const blob = new Blob([data], {type: "application/json"});
        var a = $("<a style='display: none;'/>");
        var url = window.URL.createObjectURL(blob);
        a.attr("href", url);
        a.attr("download", name);
        $("body").append(a);
        a[0].click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }
    $(document).ready(async function () {
        for( let i = 0; i < localStorage.length; i++) {
            var key = localStorage.key(i);
            if(OTHER_DATA.includes(key)) continue;
            try {
                var JSONData = JSON.parse(localStorage.getItem(key))
                if(JSONData.status == 'added') {
                    date = new Date(JSONData.date);
                    newLine = '<tr id="element' + key + '"><td>' + JSONData.structure + '</td>';
                    newLine += '<td>' + date.toLocaleDateString() + '</td>';
                    newLine += '<td><a target="_blanc" href="https://hal.archives-ouvertes.fr/'+ key +'">';
                    newLine += key + '</a></td>';
                    newLine += '<td><a target="_blanc" href="https://dx.doi.org/'+ JSONData.DOI +'">';
                    newLine += JSONData.DOI + '</a></td></tr>';
                    await $('#addedTableBody').append(newLine);
                }
            } catch(error) {
                OTHER_DATA.push(key);
            }
        }
        var historicDataTable = $('#addHistoy').DataTable({
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'csv'
                },
                {
                text: 'Vider',
                    action: function ( e, dt, node, config ) {
                        for(let i = 0; i < localStorage.length; i++) {
                            var key = localStorage.key(i);
                            if(OTHER_DATA.includes(key)) continue;
                            var JSONData = JSON.parse(localStorage.getItem(key))
                            if(JSONData.status == 'added') {
                                removeLocalKey = halid => {
                                    return function(data) {
                                        localStorage.removeItem(halid);
                                        historicDataTable.row($('#element' + halid)).remove().draw();
                                        removedRow.push('element' + halid);
                                    }
                                }
                                $.ajax({
                                    type: 'post',
                                    url: 'api/ignorelist.php',
                                    data: {
                                        action:'remove',
                                        halid: key,
                                    },
                                    success: removeLocalKey(key)
                                })
                            }
                        }
                    }
                }
            ]
        });
    });
</script>
</html>
