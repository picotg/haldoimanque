<?php
namespace uga\idoine;
/**
 * 
 * Gestion des erreurs
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

?>
<!DOCTYPE html>
<html>
<head>
    <title>idOIne - Gestion des éléments ignore</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bm/dt-1.12.1/b-2.2.3/b-html5-2.2.3/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bm/dt-1.12.1/b-2.2.3/b-html5-2.2.3/datatables.min.js"></script>
   <link rel="icon" type="type/ico" href="favicon.ico" />
</head>
<body>
<?php require "navbar.php" ?>
<div class = "container">
    <h1 class="title">Historique des Erreurs</h1>
    <br>
    <table id="addHistoy" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Structure</th>
                <th>Document Hal</th>
                <th>Message d'erreur</th>
                <th>DOI</th>
                <th>télécharger</th>
            </tr>
        </thead>
        <tbody id="addedTableBody">
        </tbody>
    </table>
    <div id="errorListDialog" title="Typologie des erreurs" style="display:none" class="container">
    <h2>1) TEI mal formaté</h2>
    Le fichier généré n'est pas correct. L'appli ne peut pas ajouter le TEI. Vous pouvez le modifier directement sur Hal.

    <h2>2) Problème de droit</h2>
    Vous n'avez pas la propriété sur le dépôt Hal.

    <h2>3) {"duplicate-entry":{"#halid":{"doi":"1.0"}}}</h2>
    Le document est sans doute un doublon.

    <h2>4) erreur non identifier</h2>
    L'erreur n'a pas été reconnue.
    </div>
    <a id="showErrorList">Afficher la typologie des erreurs.</a>
</div>
</body>
<script>
    // liste des données dans local storage n'éatant pas des document
    const OTHER_DATA = ['portail'];

    $("#showErrorList").click(ev => {
        $("#errorListDialog").dialog ({
            width: 800,
            position: {my: 'center', at: "center", of: window},
        });
    })
    function downData(data, name) {
        const blob = new Blob([data], {type: "application/json"});
        var a = $("<a style='display: none;'/>");
        var url = window.URL.createObjectURL(blob);
        a.attr("href", url);
        a.attr("download", name);
        $("body").append(a);
        a[0].click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }
    var removedRow = [];
    $(document).ready(function () {
        var historicDataTable = $('#addHistoy').DataTable({
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4 ]
                    },
                },
                {
                    text: 'Vider',
                    action: function ( e, dt, node, config ) {
                        for(let i = 0; i < localStorage.length; i++) {
                            var key = localStorage.key(i);
                            if(OTHER_DATA.includes(key)) continue;
                            var JSONData = JSON.parse(localStorage.getItem(key))
                            if(JSONData.status == 'error') {
                                removeLocalKey = halid => {
                                    return function(data) {
                                        localStorage.removeItem(halid);
                                        historicDataTable.row($('#element' + halid)).remove().draw();
                                        removedRow.push('element' + halid);
                                    }
                                }
                                $.ajax({
                                    type: 'post',
                                    url: 'api/ignorelist.php',
                                    data: {
                                        action:'remove',
                                        halid: key,
                                    },
                                    success: removeLocalKey(key)
                                })
                            }
                        }
                    }
                }
            ],
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if(removedRow.includes(nRow.id)) {
                    ignoreDataTable.row(nRow).remove().draw();
                }
            }
        });
    });
    for( let i = 0; i < localStorage.length; i++){
        var key = localStorage.key(i);
        if(OTHER_DATA.includes(key)) continue;
        try {
            var JSONData = JSON.parse(localStorage.getItem(key))
            if(JSONData.status == 'error') {
                var date = new Date(JSONData.date);
                var newLine = '<tr id="element' + key + '"><td>' + date.toLocaleDateString() + '</td>';
                newLine += '<td>' + JSONData.structure + '</td>';
                newLine += '<td><a target="_blanc" href="https://hal.archives-ouvertes.fr/'+ key +'">'
                newLine += key + '</a></td>';
                newLine += '<td>' + JSONData.errorMessage + '</td>';
                newLine += '<td><a target="_blanc" href="https://dx.doi.org/'+ JSONData.DOI +'">'
                newLine += JSONData.DOI + '</a></td>';
                newLine += '<td><a class="downResponse" data-halid="'+ key +'">réponse</a>'
                newLine += '</td></tr>';
                $('#addedTableBody').append(newLine);
            }
        } catch(error) {
            OTHER_DATA.push(key);
        }
    }
    $('.downResponse').click(function(ev) {
        ev.preventDefault();
        var halid = $(this).data('halid');
        downData(JSON.parse(JSON.parse(localStorage.getItem(halid)).response).response, halid+'.error.xml');
    })
</script>
</html>
